import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:geocoder/geocoder.dart';
import 'package:intl/intl.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:location/location.dart';
class LocationPage extends StatefulWidget {
  const LocationPage({Key? key}) : super(key: key);

  @override
  State<LocationPage> createState() => _LocationPageState();
  
}

class _LocationPageState extends State<LocationPage> {
   Completer<GoogleMapController> _controller = Completer();
Location location = Location();
  late LocationData _currentPosition;
   late String _address,_dateTime;
   double zoom = 6.0;
  static double? lat = 35.03887258086249;
  static double? lng= 39.1850783637785;
   
   LatLng _initialcameraposition = LatLng(lat!, lng!);


  @override
  Widget build(BuildContext context) {
    return Scaffold(appBar: AppBar(
      actions:[IconButton(onPressed: ()async{ await getLoc();},icon: Icon(CupertinoIcons.location_circle))],
      title: Text('My Map',style :GoogleFonts.lato()),
      centerTitle: false,
      leading: IconButton(icon: Icon(CupertinoIcons.arrow_left),onPressed: (){
      Navigator.of(context).pop();
    },),),
    body:GoogleMap(
      zoomControlsEnabled: false,
      
      
              initialCameraPosition: CameraPosition(target: _initialcameraposition,zoom:  zoom),
        onMapCreated: (GoogleMapController controller) {
          _controller.complete(controller);
        },
    ),
    );
  }
  getLoc() async{
    bool _serviceEnabled;
    PermissionStatus _permissionGranted;

    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return;
      }
    }

    _currentPosition = await location.getLocation();
    
    location.onLocationChanged.listen((LocationData currentLocation) {
      print("${currentLocation.longitude} : ${currentLocation.longitude}");
      setState(() {
        _currentPosition = currentLocation;
      lat=_currentPosition.latitude;
      lng=_currentPosition.longitude;
        zoom=10;
     
   ;
   
         
          });
        });
        print(' $lat dasjldaldja $lng');
      }

  
    
  




}


 