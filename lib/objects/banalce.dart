// To parse this JSON data, do
//
//     final balance = balanceFromJson(jsonString);

import 'dart:convert';

Balance balanceFromJson(String str) => Balance.fromJson(json.decode(str));

String balanceToJson(Balance data) => json.encode(data.toJson());

class Balance {
    Balance({
       required this.success,
       required this.data,
       required this.message,
    });

    bool success;
    int data;
    String? message;

    factory Balance.fromJson(Map<String, dynamic> json) => Balance(
        success: json["success"],
        data: json["data"],
        message: json["message"],
    );

    Map<String, dynamic> toJson() => {
        "success": success,
        "data": data,
        "message": message,
    };
}
