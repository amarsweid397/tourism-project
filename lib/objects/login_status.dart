// To parse this JSON data, do
//
//     final loginStatus = loginStatusFromJson(jsonString);

import 'dart:convert';

LoginStatus loginStatusFromJson(String str) => LoginStatus.fromJson(json.decode(str));

String loginStatusToJson(LoginStatus data) => json.encode(data.toJson());

class LoginStatus {
    LoginStatus({
  required      this.success,
  required      this.data,
  required      this.message,
    });

    bool success;
    Data data;
    String message;

    factory LoginStatus.fromJson(Map<String, dynamic> json) => LoginStatus(
        success: json["success"],
        data: Data.fromJson(json["data"]),
        message: json["message"],
    );

    Map<String, dynamic> toJson() => {
        "success": success,
        "data": data.toJson(),
        "message": message,
    };
}

class Data {
    Data({
       required this.name,
    });

    String name;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        name: json["name"],
    );

    Map<String, dynamic> toJson() => {
        "name": name,
    };
}
