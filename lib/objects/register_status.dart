// To parse this JSON data, do
//
//     final registerstatus = registerstatusFromJson(jsonString);

import 'dart:convert';

Registerstatus registerstatusFromJson(String str) => Registerstatus.fromJson(json.decode(str));

String registerstatusToJson(Registerstatus data) => json.encode(data.toJson());

class Registerstatus {
    Registerstatus({
      required  this.success,
     required   this.data,
     required   this.message,
    });

    bool? success;
    Data? data;
    String? message;

    factory Registerstatus.fromJson(Map<String, dynamic> json) => Registerstatus(
        success: json["success"],
        data: Data.fromJson(json["data"]),
        message: json["message"],
    );

    Map<String, dynamic> toJson() => {
        "success": success,
        "data": data!.toJson(),
        "message": message,
    };
}

class Data {
    Data({
  required      this.token,
  required      this.name,
    });

    String token;
    String name;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        token: json["token"],
        name: json["name"],
    );

    Map<String, dynamic> toJson() => {
        "token": token,
        "name": name,
    };
}
