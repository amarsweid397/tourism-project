// To parse this JSON data, do
//
//     final tripsObj = tripsObjFromJson(jsonString);

import 'dart:convert';

List<TripsObj> tripsObjFromJson(String str) => List<TripsObj>.from(json.decode(str).map((x) => TripsObj.fromJson(x)));

String tripsObjToJson(List<TripsObj> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class TripsObj {
    TripsObj({
    required    this.id,
    required    this.name,
   required     this.phoneNumber,
   required     this.evaluation,
        this.createdAt,
        this.updatedAt,
    });

    int id;
    String name;
    String phoneNumber;
    int evaluation;
    dynamic createdAt;
    dynamic updatedAt;

    factory TripsObj.fromJson(Map<String, dynamic> json) => TripsObj(
        id: json["id"],
        name: json["name"],
        phoneNumber: json["phoneNumber"],
        evaluation: json["evaluation"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "phoneNumber": phoneNumber,
        "evaluation": evaluation,
        "created_at": createdAt,
        "updated_at": updatedAt,
    };
}
