// To parse this JSON data, do
//
//     final offertUser = offertUserFromJson(jsonString);

import 'dart:convert';

OffertUser offertUserFromJson(String str) => OffertUser.fromJson(json.decode(str));

String offertUserToJson(OffertUser data) => json.encode(data.toJson());

class OffertUser {
    OffertUser({
     required   this.success,
     required   this.data,
     required   this.message,
    });

    bool success;
    List<Datum> data;
    String message;

    factory OffertUser.fromJson(Map<String, dynamic> json) => OffertUser(
        success: json["success"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        message: json["message"],
    );

    Map<String, dynamic> toJson() => {
        "success": success,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "message": message,
    };
}

class Datum {
    Datum({
      required this.tripName,
    required    this.cost,
    required    this.startsAt,
    required    this.endsAt,
     required   this.costAfterOffer,
    });

    String tripName;
    int cost;
    DateTime startsAt;
    DateTime endsAt;
    int costAfterOffer;

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        tripName: json["trip name"],
        cost: json["cost"],
        startsAt: DateTime.parse(json["starts_at"]),
        endsAt: DateTime.parse(json["ends_at"]),
        costAfterOffer: json["cost_after_offer"],
    );

    Map<String, dynamic> toJson() => {
        "trip name": tripName,
        "cost": cost,
        "starts_at": "${startsAt.year.toString().padLeft(4, '0')}-${startsAt.month.toString().padLeft(2, '0')}-${startsAt.day.toString().padLeft(2, '0')}",
        "ends_at": "${endsAt.year.toString().padLeft(4, '0')}-${endsAt.month.toString().padLeft(2, '0')}-${endsAt.day.toString().padLeft(2, '0')}",
        "cost_after_offer": costAfterOffer,
    };
}
