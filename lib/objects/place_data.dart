// To parse this JSON data, do
//
//     final placesData = placesDataFromJson(jsonString);

import 'dart:convert';

 placesDataFromJson(String str) => PlacesData.fromJson(json.decode(str));

String placesDataToJson(PlacesData data) => json.encode(data.toJson());

class PlacesData {
    PlacesData({
  required        this.places,
    });

    List<List<dynamic>> places;

    factory PlacesData.fromJson(Map<String, dynamic> json) => PlacesData(
        places: List<List<dynamic>>.from(json["places"].map((x) => List<dynamic>.from(x.map((x) => x)))),
    );

    Map<String, dynamic> toJson() => {
        "places": List<dynamic>.from(places.map((x) => List<dynamic>.from(x.map((x) => x)))),
    };
}

class PurplePlace {
    PurplePlace({
  required      this.url,
    });

    String url;

    factory PurplePlace.fromJson(Map<String, dynamic> json) => PurplePlace(
        url: json["URL"],
    );

    Map<String, dynamic> toJson() => {
        "URL": url,
    };
}

class FluffyPlace {
    FluffyPlace({
      required  this.name,
     required   this.id,
    });

    String name;
    int id;

    factory FluffyPlace.fromJson(Map<String, dynamic> json) => FluffyPlace(
        name: json["name"],
        id: json["id"],
    );

    Map<String, dynamic> toJson() => {
        "name": name,
        "id": id,
    };
}
