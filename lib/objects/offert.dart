// To parse this JSON data, do
//
//     final offer = offerFromJson(jsonString);

import 'dart:convert';

List<Offer> offerFromJson(String str) => List<Offer>.from(json.decode(str).map((x) => Offer.fromJson(x)));

String offerToJson(List<Offer> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Offer {
    Offer({
     required   this.id,
    required    this.name,
     required   this.startsAt,
    required    this.endsAt,
     required   this.percentage,
        this.createdAt,
        this.updatedAt,
    });

    int id;
    String name;
    DateTime startsAt;
    DateTime endsAt;
    int percentage;
    dynamic createdAt;
    dynamic updatedAt;

    factory Offer.fromJson(Map<String, dynamic> json) => Offer(
        id: json["id"],
        name: json["name"],
        startsAt: DateTime.parse(json["starts_at"]),
        endsAt: DateTime.parse(json["ends_at"]),
        percentage: json["percentage"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "starts_at": "${startsAt.year.toString().padLeft(4, '0')}-${startsAt.month.toString().padLeft(2, '0')}-${startsAt.day.toString().padLeft(2, '0')}",
        "ends_at": "${endsAt.year.toString().padLeft(4, '0')}-${endsAt.month.toString().padLeft(2, '0')}-${endsAt.day.toString().padLeft(2, '0')}",
        "percentage": percentage,
        "created_at": createdAt,
        "updated_at": updatedAt,
    };
}
