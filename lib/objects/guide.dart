// To parse this JSON data, do
//
//     final guide = guideFromJson(jsonString);

import 'dart:convert';

List<Guide> guideFromJson(String str) => List<Guide>.from(json.decode(str).map((x) => Guide.fromJson(x)));

String guideToJson(List<Guide> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Guide {
    Guide({
     required   this.id,
      required  this.name,
      required  this.phoneNumber,
      required  this.evaluation,
        this.createdAt,
        this.updatedAt,
    });

    int id;
    String name;
    String phoneNumber;
    int evaluation;
    dynamic createdAt;
    dynamic updatedAt;

    factory Guide.fromJson(Map<String, dynamic> json) => Guide(
        id: json["id"],
        name: json["name"],
        phoneNumber: json["phoneNumber"],
        evaluation: json["evaluation"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "phoneNumber": phoneNumber,
        "evaluation": evaluation,
        "created_at": createdAt,
        "updated_at": updatedAt,
    };
}
