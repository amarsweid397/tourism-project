// To parse this JSON data, do
//
//     final endTrips = endTripsFromJson(jsonString);

import 'dart:convert';

EndTrips endTripsFromJson(String str) => EndTrips.fromJson(json.decode(str));

String endTripsToJson(EndTrips data) => json.encode(data.toJson());

class EndTrips {
    EndTrips({
        required this.trips,
    });

    List<Trip> trips;

    factory EndTrips.fromJson(Map<String, dynamic> json) => EndTrips(
        trips: List<Trip>.from(json["trips"].map((x) => Trip.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "trips": List<dynamic>.from(trips.map((x) => x.toJson())),
    };
}

class Trip {
    Trip({
        required this.name,
    });

    String name;

    factory Trip.fromJson(Map<String, dynamic> json) => Trip(
        name: json["name"],
    );

    Map<String, dynamic> toJson() => {
        "name": name,
    };
}
