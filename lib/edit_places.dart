import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:focused_menu/focused_menu.dart';
import 'package:focused_menu/modals.dart';
import 'package:udemy_flutter/services/http.dart';

import 'objects/place_data.dart';

class EditPlaces extends StatefulWidget {
  final index ;

  const EditPlaces({Key? key, required this.index}) : super(key: key);

  @override
  State<EditPlaces> createState() => _EditPlacesState();
}

class _EditPlacesState extends State<EditPlaces> {

  bool isLoading = true ;
List<dynamic>? placesData ; 
  HttpService service = new HttpService(); 
  int length = 0 ; 
  int type = 1; 
  getData()async{
   placesData=  await service.getPlacesData(type, 1);
    if(placesData!=null){
      setState(() {
        isLoading = false ; 
      length = placesData!.length ; 
      });
    

    }
  }
  int findIndex (String? str ){
    int x =0 ;
    for(var i =0 ; i <items.length;i++){
      if (str==items[i])
      x=i ; 
    }
    return x ; 
    
  }
@override
  void initState() {
    // TODO: implement initState
    super.initState();
    getData();
  }
  String dropdownvalue = 'Choose places?';

  @override
  var items = [
    'Choose places?',
    "touristic places",
    "Historical places",
       "religious places",
    "Places of entertainment",
 
  ];

  @override
  Widget build(BuildContext context) {
    var index;
    return SafeArea(
        child: Scaffold(
      body: Container(
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    icon: Icon(
                      CupertinoIcons.back,
                      size: 25,
                      color: Colors.black,
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                  Text(
                    'Editing places:',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),
                  ),
                  Expanded(child: Center()),
                  CircleAvatar(
                    child: Image.network(
                        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQgRUJ10AIVbVTbcH70E3IKh2X_XcHzLuITVA&usqp=CAU',
                        fit: BoxFit.cover),
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Icon(Icons.sort),
                  DropdownButton(
                    value: dropdownvalue,
                    items: items.map((String items) {
                      return DropdownMenuItem(
                        // style: TextStyle(
                        //     color: Colors.black,
                        //     fontSize: 20,
                        //     fontWeight: FontWeight.bold),
                        value: items,
                        child: Text(items),
                      );
                    }).toList(),
                    onChanged: (String? newValue) async{

                      setState(() {
                        dropdownvalue = newValue!;
                        type = findIndex(newValue);
             isLoading= true;          
                      });
                      await getData();
                      
                    },
                  ),
                  // DropdownButton(
                  //
                  //     style: TextStyle(fontSize: 15, color: Colors.black),
                  //     icon: Icon(Icons.keyboard_arrow_down),
                  //     underline: Container(
                  //       color: Colors.white,
                  //     ),
                  //     items: ["Featured", "Most Rated", "Recent", "Popular"]
                  //       .map<DropdownMenuItem>((e) =>
                  //        DropdownMenuItem(child: Text(e))).toList(),
                  //     onChanged: (newItem) {}),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Expanded(
                child:
                
                
                
                 Visibility(

                  replacement: Center(child: CircularProgressIndicator()),
                  visible: !isLoading,
                   child: GridView.builder(
                    itemCount: length,
                      physics: BouncingScrollPhysics(),
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 1),
                            
                            itemBuilder: (context,index)=>Container(
                                child: FocusedMenuHolder(
                                  menuWidth:
                                      MediaQuery.of(context).size.width * 0.50,
                                  blurSize: 4.0,
                                  menuItemExtent: 45,
                                  menuBoxDecoration: BoxDecoration(
                                      color: Colors.grey,
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(15.0))),
                                  duration: Duration(milliseconds: 100),
                                  animateMenuItems: true,
                                  blurBackgroundColor: Colors.black54,
                                  openWithTap:
                                      true, // Open Focused-Menu on Tap rather than Long Press
                                  menuOffset:
                                      10.0, // Offset value to show menuItem from the selected item
                                  bottomOffsetHeight:
                                      80.0, // Offset height to consider, for showing the menu item ( for example bottom navigation bar), so that the popup menu will be shown on top of selected item.
                                  menuItems: <FocusedMenuItem>[
                                    // Add Each FocusedMenuItem  for Menu Options
                                    FocusedMenuItem(
                                        title: Text("Open"),
                                        trailingIcon: Icon(Icons.open_in_new),
                                        onPressed: () {
                                          //  Navigator.push(context, MaterialPageRoute(builder: (context)=>ScreenTwo()));
                                        }),
                                    // FocusedMenuItem(title: Text("Share"),trailingIcon: Icon(Icons.share) ,onPressed: (){}),
                               
                                    FocusedMenuItem(
                                        title: Text(
                                          "Delete",
                                          style:
                                              TextStyle(color: Colors.redAccent),
                                        ),
                                        trailingIcon: Icon(
                                          Icons.delete,
                                          color: Colors.redAccent,
                                        ),
                                        onPressed: () async{

                                          await service.deletePlace(placesData![index][0]['id'],);
                                          setState(() {
                                            isLoading=true;
                                            
                                          });
                                          await getData();
                                        }),
                                  ],
                                  onPressed: () {},
                                  child: Container(
                                    margin: EdgeInsets.only(top: 30),
                                    child: Card(
                                      clipBehavior: Clip.antiAlias,
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(35)),
                                      child: Stack(
                                        alignment: Alignment.center,
                                        children: [
                                          Container(
                                            height: 600,
                                            decoration: BoxDecoration(
                                                image: DecorationImage(
                                                    image: NetworkImage(
                                                        'http://picsum.photos/500/500?random=$index'),
                                                    fit: BoxFit.cover)),
                                          ),
                                          Positioned(
                                              bottom: 16,
                                              left: 16,
                                              right: 16,
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                placesData![index][0]['name'],
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        color: Colors.white,
                                                        fontSize: 24),
                                                  ),
                                                  Row(
                                                    children: [
                                                      Icon(
                                                        Icons.star,
                                                        color: Colors.yellow,
                                                        size: 25,
                                                      ),
                                                      Icon(
                                                        Icons.star,
                                                        color: Colors.yellow,
                                                        size: 25,
                                                      ),
                                                      Icon(
                                                        Icons.star,
                                                        color: Colors.yellow,
                                                        size: 20,
                                                      ),
                                                      Icon(
                                                        Icons.star_half,
                                                        color: Colors.yellow,
                                                        size: 20,
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              )),
                                        ],
                                      ),
                                      elevation: 10,
                                      shadowColor: Colors.grey.withOpacity(.4),
                                    ),
                                  ),
                    ),
                               ),
                             ),
                 )),
         ] ),
        ),
      ),
    ));
  }
}
