
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:udemy_flutter/services/http.dart';

import 'objects/end_trips.dart';

class TripsMade extends StatefulWidget {
  const TripsMade({ Key? key }) : super(key: key);

  @override
  State<TripsMade> createState() => _TripsMadeState();
}

class _TripsMadeState extends State<TripsMade> {
  int index=0;
bool isLoading = true;
List<Trip>? myTrips ;
HttpService service = new HttpService();
  @override
  void initState(){
    super.initState();
    getTrips();
  }
getTrips()async{
  myTrips= await service.getEndTrips();
  if (myTrips!=null){
    setState(() {
      
      isLoading= false ; 
    });
  }
}
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(image: DecorationImage(image: NetworkImage('https://i.pinimg.com/236x/77/4f/fc/774ffc6d4bcf04f29648d98059ce4844.jpg')
          ,fit: BoxFit.cover,
          colorFilter: ColorFilter.mode(Colors.black.withOpacity(0.2), BlendMode.darken)
      )
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          leading: IconButton(icon:Icon(CupertinoIcons.back,size: 35,color: Colors.white,),onPressed: (){
            Navigator.of(context).pop();
          },),
        ),

        //العنوان

        body: 
        
        Visibility(
          visible: !isLoading,
          replacement: Center(child: CircularProgressIndicator()),
          child: ListView(
              children: [
                Center(child: Text(".. Trips Made ..",
                  style: TextStyle(fontWeight: FontWeight.bold,fontSize: 50,color: Colors.white,fontFamily: 'Courgette'),),)
                ,
                SizedBox(height: 50,),
        
                Padding(padding:
                const EdgeInsets.symmetric(horizontal: 10,),
                  child: ListView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,

                      itemBuilder: (context,index)=>
                        bild(content: myTrips![index].name)
                      ,
                   
                      itemCount:myTrips?.length),
        
                ),
        
        
              ]
        
          ),
        ),
      ),
    );


  }
  Widget bild({required String content})=> Padding(
    padding: const EdgeInsets.symmetric(vertical: 8),
    child: Container(
      child: Card(
        color: Colors.white,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15)
        ),
        elevation: 10,
        shadowColor: Colors.blue,
        child: ListTile(
          leading: CircleAvatar(
            backgroundColor: Colors.white38,
            radius: 18,backgroundImage:NetworkImage('https://static.thenounproject.com/png/2548936-200.png') ,
        ),title: Row(
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(content,
                  style: TextStyle(fontSize:18,fontWeight: FontWeight.bold,color: Colors.blue),),
             
              ],
            ),
            SizedBox(width: 8,),
            Icon(Icons.check_circle,color: Colors.green,)
          ],
        ),

        ),

      ),
    ),
  );

}
