import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_swiper_view/flutter_swiper_view.dart';
import 'package:udemy_flutter/services/http.dart';

class ArchaPage extends StatefulWidget {
  const ArchaPage({Key? key}) : super(key: key);

  @override
  State<ArchaPage> createState() => _ArcahPageState();
}

class _ArcahPageState extends State<ArchaPage> {
   String dropdownvalue =  "Aleppo";
List<dynamic>?hotels ;
int length = 0 ; 
bool isLoading = true ; 
HttpService service = new HttpService();

getData()async{
hotels = await  service.getPlacesData(2, xx);
if(hotels!=null){
  setState(() {
    isLoading = false;
    length = hotels!.length; 

  });
}
}
int xx= 3 ; 
int getIndex(String x ){
  int y =0 ; 
  if(x== "Aleppo") y=3;
  else if (x=="Lattakia")y=4;
  else if (x=="Homs")y=5;
  else if (x== "Hama")y=7;
  else y=5;
  return  y; 
}
@override
   initState() {
  
    super.initState();
    getData();
  }
  @override

   var items = [    


 "Aleppo",
 "Lattakia",
 "Homs",
 "Hama",
 "Tartus",];

  Widget build(BuildContext context) {
    Size _size = MediaQuery.of(context).size;
     return Scaffold(
        appBar: AppBar(
            title: Text('Famous Archaeolgoiacl  ',style :TextStyle(fontSize:25,fontWeight: FontWeight.bold),
      textAlign:TextAlign.center,),
      backgroundColor: Colors.teal,
            leading: IconButton(icon:Icon(CupertinoIcons.back,size: 35,color: Colors.white,),onPressed: (){
              Navigator.of(context).pop();
            },),
            actions: [
              IconButton(
               onPressed: (){
             showDialog(context: context, builder: (BuildContext context){
                  return AlertDialog(
                    title: Text('This interface displays the most famous hotels in Syria, as it displays the pictures of each hotel with the name, rating, number and details for each hotel separately',
                    style: TextStyle(fontSize:25,fontWeight: FontWeight.bold,color:Colors.teal  ),),
                    
                    );

                });


          },
              
               icon: Icon(CupertinoIcons.question_circle,color: Colors.white,size: 30,))
            
          ],),
          body: Container(
            child: ListView(
              shrinkWrap: true,
              children: [
               Padding(
                 padding: const EdgeInsets.symmetric(horizontal: 20),
                 child: DropdownButton(
                      style:TextStyle(color: Colors.black,fontSize:25,fontWeight:FontWeight.bold  ) ,
                    // Initial Value
                    value: dropdownvalue,
                      
                    // Down Arrow Icon
                    icon: const Icon(Icons.keyboard_arrow_down,color: Colors.white,size:25),    
                      
                    // Array list of items
                    items: items.map((String items) {
                      return DropdownMenuItem(
                        value: items,
                        child: Text(items),
                      );
                    }).toList(),
                    // After selecting the desired option,it will
                    // change button value to selected value
                    onChanged: (String? newValue) async{ 
                      setState(() {
                        dropdownvalue = newValue!;
                        xx = getIndex(newValue);
                        isLoading = true;
                      });
                      await getData();
                    },
                  ),
               ),
          
               Container(
                  padding: EdgeInsets.symmetric(vertical: 15),
                  child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
             child: Visibility(visible: !isLoading,
             replacement:Center(child: CircularProgressIndicator(),),
             child: ListView.builder(
              shrinkWrap: true,
              itemCount: length,
              itemBuilder: (context,index)=>  Swiperpp(
                
                index :index ,
                id : hotels![index][0]['id'],
                imageSrc: 'https://picsum.photos/500/500?random=2', content1: hotels![index][0]['name'], content2: 'The Rating', size: _size,
                 iconz:Row(children: [Icon(Icons.star,color: Colors.yellow,size: 20,),
                   Icon(Icons.star,color: Colors.yellow,size: 20,),
                   Icon(Icons.star_half,color: Colors.yellow,size: 20,)],),num: hotels![index][0]['phoneNumber'],content3:'Details :',icon: Icons.details ),
                ),)
                  ),
          
               )
          
          
          
          
            ],),
          ),


      );
      
    
  }
Padding Swiperpp ({required Row iconz,required String imageSrc,required String content1,required String content2,required Size size,required String num,required IconData icon,required String content3, required int index , required int  id})
{
  return Padding(
    padding: const EdgeInsets.symmetric(horizontal: 5,vertical: 5),
    child: Column(
          children: [
            Stack(
              children: [
               Container(
                width: size.width,
                height: size.height/4, // لل swiper
                child: Swiper(
                        itemCount:5,
                        //autoplay: true,
                        layout: SwiperLayout.CUSTOM,
                        customLayoutOption: CustomLayoutOption(startIndex: 0,stateCount: 3),

                        itemWidth: double.infinity,
                        pagination: new SwiperPagination(),
                        //control: new SwiperControl(),
                        loop: true,
                        itemBuilder: (BuildContext context,int index) => ClipRRect(
                          borderRadius: BorderRadius.only(bottomLeft: Radius.circular(10),
                          bottomRight: Radius.circular(10)),
                          child: Container(
                        decoration: BoxDecoration( image:DecorationImage(image: 
                         NetworkImage('https://picsum.photos/500/500?random=$index'),
                            fit: BoxFit.fill),),
                          ),
                        ),
                      ),
              ),
              // للنص بال swiper
              Positioned(
                   bottom: 20,
                    left: 15,
                   // child: Title(
                        //color: Colors.black,
                        child: Column(children: [  Text(content1,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 30,
                                color: Colors.white,backgroundColor:Colors.black.withOpacity(0.7) ),
                              
                                ),
                             SizedBox(height: 5,),
                                Text(num??"Not Added",textAlign: TextAlign.left,style: TextStyle(fontSize: 20,fontWeight:FontWeight.bold ,color: Colors.white,backgroundColor:Colors.black.withOpacity(0.7)),
                                ),
                               
                               Row(children: [Text(content3,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 22,
                                color: Colors.white,backgroundColor:Colors.black.withOpacity(0.7) ),
                              
                                ),
                                IconButton(onPressed: ()async{
                                  String data =await service.getPlaceDetails(id)??' ';
                                              
                showDialog(context: context, builder: (BuildContext context){
                  return AlertDialog(
                    title: Text(
                  data  ,style: TextStyle(fontSize:20,fontWeight: FontWeight.bold,color:Colors.teal  ),),

                    );

                });
                                }, icon: Icon(icon,size:30 ,color:Colors.teal ,) )
                                ],)
                        ])           
                           ),
                                 
              ],
            ),
            SizedBox(height:5 ,),
             Row(
               children: [
                 SizedBox(width: 15,),
                 Text(content2,textAlign: TextAlign.left,style: TextStyle(fontSize: 25,fontWeight:FontWeight.bold ,color: Colors.black),),SizedBox(width: 5,),
                 //Icon(Icon(),color: Colors.yellow,size: 20,),
                 iconz,
               ],
             ),
    
          ]
  )
  );
}
}