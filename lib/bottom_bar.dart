import 'dart:ui';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:udemy_flutter/objects/offert_taken.dart';
import 'package:udemy_flutter/offer_user.dart';
import 'package:udemy_flutter/places.dart';

import 'group_trip.dart';
import 'home_page.dart';
import 'location.dart';

class BottomBar extends StatefulWidget {
 int index  =0;
   BottomBar({required this.index});


  @override
  State<BottomBar> createState() => _BottomBarState();
}

class _BottomBarState extends State<BottomBar> {
  late int _index;
   @override
  void initState() {
    // TODO: implement initState
    super.initState();
_index =widget.index;
  }
   final List pages = [
HomePage(),
TourismPlaces(),
OffersUser(),

LocationPage(),
GroupTripPage(),
Container(child: Center(child: Text('Language'),),),




   ];
  @override
  
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true, // لتوضح الشفافية
      body: pages[_index],
      bottomNavigationBar: Container(
        child: Padding(
        padding: EdgeInsets.only(left: 25,right:25,bottom: 40),
        child:ClipRRect(
          borderRadius: BorderRadius.circular(25),
          child: BackdropFilter(
            filter: ImageFilter.blur(sigmaX: 10,sigmaY:10),
         
              child: Container(
            height: 60,
            color: Colors.white.withOpacity(0.75),
            child: 
            Row(children: [
            Expanded(
              child: NavButton(
              
                icon: CupertinoIcons.home,
                content: 'Home'
                ,index:0
              ),
            ),
            Expanded(
              child: NavButton(
                
                icon: Icons.location_city,
                content: 'Places'
                ,index:1
              ),
            ),
            Expanded(
              child: NavButton(
               
                icon: CupertinoIcons.cart,
                content: 'Offers'
                ,index:2
              ),
            ),
            
            
            Expanded(
              child: NavButton(
                 
                  icon: CupertinoIcons.location_circle_fill,
                  content: 'Location'
                  ,index:3
                ),
            ),
            
            ],),
          )
          ),
        )
        ),
      ),
      
    );
  }

  FlatButton NavButton({
    required int index  ,required IconData icon ,required String content
  }) {
    return FlatButton(onPressed: (){
      setState(() {
        _index=index;
      });

          }, child: Column(
            mainAxisSize: MainAxisSize.min, //
            children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [Icon(icon,color: _index==index?Colors.blueAccent:null,)],),
            Text(_index==index?content:'',style: TextStyle(color: Colors.blueAccent),textAlign:TextAlign.left ,)
          ],));
  }
}