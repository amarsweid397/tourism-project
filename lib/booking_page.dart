import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:udemy_flutter/modules/Trips.dart';
import 'package:udemy_flutter/services/http.dart';



class BookingPage extends StatefulWidget {
   Trips trip ; 
   BookingPage({ required this.trip}) ;
  
  @override
    State<BookingPage>createState() => _BookingPage();
}

class _BookingPage extends State<BookingPage> {


  @override
  Widget build(BuildContext context) {

    Size _size = MediaQuery.of(context).size;
    HttpService service = new HttpService();
    return Scaffold(
     
      // الازرار في الاسفل
      bottomNavigationBar: BottomAppBar(
        child: Padding(
          padding: const EdgeInsets.only(bottom: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
            RaisedButton(
              padding: EdgeInsets.only(bottom:5,left: 10,right: 10,top: 5 ),
              color: Colors.redAccent,
              child: Text('Bank Booking',style: TextStyle(color: Colors.white,fontSize:20 ),),
              

              onPressed: (
              )
              async
              {
              await service.bookTrip(widget.trip.data.id);
                showDialog(context: context, builder: (BuildContext context){
                  return AlertDialog(
                    title: Text('Booked ..',style: TextStyle(fontSize:30,fontWeight: FontWeight.bold,color:Colors.green  ),),
                    // content:Image(image: AssetImage('images/download.jpg'),width: 5,height:5)
                    content: Icon(CupertinoIcons.smiley_fill,size:60 ,color: Colors.green,),
                    );

                });
                
              } 
              ),
      RaisedButton(
          child: Text('Local Booking',style: TextStyle(color: Colors.white,fontSize:20)),
              padding: EdgeInsets.only(bottom:5,left: 10,right: 10,top: 5 ),
              color: Colors.green,
          onPressed: (){
             showDialog(context: context, builder: (BuildContext context){
                  return AlertDialog(
                    title: Text('The Company Number 0991055612',style: TextStyle(fontSize:30,fontWeight: FontWeight.bold,color:Colors.red[300]  ),),
                    // content:Image(image: AssetImage('images/download.jpg'),width: 5,height:5)
                    content: Icon(CupertinoIcons.smiley,size:60 ,color: Colors.teal,),
                    );

                });


          }
          )],),
        ),),
      // الصورة + النص
      body: ListView(
        scrollDirection:Axis.vertical,
        children: [
          Stack(children: [Container(
            width:_size.width ,
            height: _size.height/2.4,
          decoration: BoxDecoration( borderRadius: BorderRadius.only(bottomLeft: Radius.circular(50),
         bottomRight: Radius.circular(50))
         , image:DecorationImage(image: NetworkImage('https://picsum.photos/500/500?random=2')
          ,fit:BoxFit.cover )),
          ),
          Positioned(top: 45,left: 18,
          child:Text('Make A Booking ..', 
                            style: TextStyle(
                             fontStyle:FontStyle.italic,
                                fontWeight: FontWeight.bold,
                                fontSize: 40,
                                color: Colors.black)
                            )),

              Padding(
              padding: const EdgeInsets.only(top: 0,left: 5),
              //child: Container(
                child: Row(children: [IconButton(onPressed: (){
               Navigator.of(context).pop();
              }, icon:Icon(CupertinoIcons.back,size:30,color: Colors.white,) )],),),
          ],)
     ,
      // شرح عن الرحل
       Padding(
         padding: const EdgeInsets.only(left: 12,top: 15,right: 15),
         child: Text(widget.trip.data.details
         ,style: TextStyle(fontSize: 20),
         
         ),
       )
,
       Expanded(child: Column(children:[
           // معلعومات الرحلة
          BookingDetailsWidget(content1: 'Trip Time', content2:  Text(widget.trip.data.tripTime.toString(),style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20,color: Colors.blueAccent))),
          BookingDetailsWidget(content1: 'Trip Date', content2:  Text(widget.trip.data.startsAt.toString(),style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20,color: Colors.blueAccent))),
          BookingDetailsWidget(content1: 'Trip Cost',content2:  Text('\$ ${widget.trip.data.cost}',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20,color: Colors.blueAccent))),
         BookingDetailsWidget(content1: 'Period', content2:  Text(widget.trip.data.endsAt.toString(),style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20,color: Colors.blueAccent))),

       ]))
         
      
            ],),
    );
  }

  Widget BookingDetailsWidget({required String content1,required content2}) {
    return Padding(padding: EdgeInsets.only(top: 15,left: 10),child: Row(children: [
       Text('$content1   :',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 25),),SizedBox(width: 10,),content2,
 
     
     ],)
     
  );
  }
}