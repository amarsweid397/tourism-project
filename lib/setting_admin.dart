// import 'package:flutter/material.dart';
// import 'package:flutter_settings_screens/flutter_settings_screens.dart';
// import 'package:udemy_flutter/widget_pro/icon_widget.dart';
// class SettingAdmin extends StatelessWidget {
//   const SettingAdmin({Key? key}) : super(key: key);
// static const KeyDarkMode='Key_dark_mod';
// static const KeyLanguage='Key_language';
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         body: SafeArea(child: ListView(
//           padding: EdgeInsets.all(24),
//           children: [
//             SettingsGroup(title: 'General', children: [
//               buildLogout(),
//               buildDeleteAccount(),
//             ]),
//             SizedBox(height: 32,),
//             SettingsGroup(title: 'FeedBack', children: [
//               SizedBox(height: 8,),
//               buildLogout(),
//               buildDeleteAccount(),
//             ]),
//
//           ],
//         ),),
//       );
//   }
//   Widget buildDarkMode()=>SwitchSettingsTile(
//     settingKey:KeyDarkMode,
//     leading:IconWidget(icon:Icons.dark_mode,
//       color:Color(0xFF642ef3),),
//     title:'Dark Mode',
//   onChange:(isDarkMode){}
//   );
//   Widget buildLogout()=>SimpleSettingsTile(
//       leading:IconWidget(icon:Icons.logout,
//         color:Colors.blueAccent,),
//       title:'Logout',
//       onTap: (){},
//   );
//   Widget buildAccount()=>SimpleSettingsTile(
//     leading:IconWidget(icon:Icons.person,
//       color:Colors.green,),
//     title:'Account Settings',
//     subtitle: 'privacy,Security,Language',
//     child: Container(
//
//     ),
//     onTap: (){},
//   );
//   Widget buildLanguage()=>DropDownSettingsTile(
//    settingKey: KeyLanguage,
//     title:'Language',
//     selected: 1,
//    values: <int,String>{
//      1:'English',
//      2:'Arabic'
//    },
//     onChange: (language){},
//   );
//   Widget buildDeleteAccount()=>SimpleSettingsTile(
//     leading:IconWidget(icon:Icons.delete,
//       color:Colors.pink,),
//     title:'DeleteAccount',
//     onTap: (){}
//   );
// }
