import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:udemy_flutter/balance_screen.dart';
import 'package:udemy_flutter/all_trip.dart';
class SwiperScreen extends StatefulWidget {
  const SwiperScreen({Key? key}) : super(key: key);

  @override
  State<SwiperScreen> createState() => _SwiperScreenState();
}

class _SwiperScreenState extends State<SwiperScreen> {
  GlobalKey<ScaffoldState> _key = new GlobalKey<ScaffoldState>();
  final index = 0;
  final urlImages=[
    //'https://i.pinimg.com/236x/52/6e/14/526e144d98cede550d3500578bda8211.jpg',
    'https://static.hosteltur.com/app/public/uploads/img/articles/2013/09/01/M_5b14ea6fb7f86_shutterstock_HOTEL.jpg',
    'https://i.pinimg.com/564x/f4/e5/a5/f4e5a5fc974d3eb97df15ae47852fb77.jpg',
    'https://img.jakpost.net/c/2017/08/09/2017_08_09_30809_1502274587._large.jpg',
    // 'https://www.dubaidrivingcenter.net/images/offers/DDC-offers.jpg'
    'https://lp-cms-production.imgix.net/2020-11/500pxRF_82178129.jpg',
    'https://miuc.org/wp-content/uploads/2019/08/How-do-we-use-money.jpg',
    //'https://surrealhotels.com/wp-content/uploads/2014/10/Special_Offers2.jpg',
    'https://www.dubaidrivingcenter.net/images/offers/DDC-offers.jpg',
    'https://arlingtonva.s3.amazonaws.com/wp-content/uploads/sites/25/2013/12/restaurant.jpeg',
    'https://cf.ltkcdn.net/camping/images/orig/257248-1600x1030-group-camping-games-activities-adults.jpg',
    'https://lp-cms-production.s3.amazonaws.com/public/2021-07/shutterstockRF_1218546019.jpg',
  ];
  @override
  Widget build(BuildContext context) {
    Size _size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Swiper(
                  itemBuilder: (BuildContext context, int index)
                  {
                  final urlImage = urlImages[index];
                  return ClipRRect(
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(50),
                        bottomRight:Radius.circular(50)),
                    child:Image.network(
                      urlImage,
                      fit: BoxFit.cover,),
                  );
                      },
                        itemCount:urlImages.length,
                        itemHeight:340,
                        itemWidth:double.infinity,
                        viewportFraction: 0.8,
                        scale: 0.9,
                        pagination: SwiperPagination(margin:EdgeInsets.only(top: 320)),
                        layout: SwiperLayout.STACK,
                       autoplay: true,
                ),
            Padding(
              padding: const EdgeInsets.only( left:15.0,bottom: 4),
              child: Row(
                children: [
                  Text(
                    'Admin :',
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontFamily: 'OleoScriptSwashCaps',
                      fontSize: 40,
                      // color: Colors.white,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
  Widget buildR ({required String content,required IconData icon,required String images,required myFunc})=>
      Container(
        height: 75,
        child: Padding(
          padding: const EdgeInsets.all(2.0),
          child: Card(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(34)
            ),
            color: Colors.grey[300],
            elevation: 10,
            shadowColor: Colors.black,
            child: ListTile(
              leading: Padding(
                padding: const EdgeInsets.only(top: 8,left: 16),
                child: CircleAvatar(radius: 22,
                  backgroundImage: NetworkImage(images),),
              ),
              title: Text(content, style: TextStyle(
                fontWeight: FontWeight.w400,
                fontFamily: 'Courgette',
                fontSize: 30,
              ),),
              trailing: Icon(Icons.arrow_forward_ios),
              onTap: myFunc,
            ),
          ),
        ),
      );
}

