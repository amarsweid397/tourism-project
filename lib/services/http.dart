import 'dart:convert';


import 'package:dio/dio.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:udemy_flutter/objects/banalce.dart';
import 'package:udemy_flutter/objects/end_trips.dart';
import 'package:udemy_flutter/objects/guide.dart';
import 'package:udemy_flutter/objects/login_status.dart';
import 'package:udemy_flutter/objects/place_data.dart';
import 'package:udemy_flutter/objects/trips.dart';

import '../modules/Trips.dart';
import '../modules/trips_gov.dart';
import '../objects/offert.dart';
import '../objects/offert_taken.dart';
import '../objects/register_status.dart';
import '../objects/user.dart';
class HttpService{
 String baseUrl ='http://192.168.43.40:8080/api';
//  String baseUrl ='http://192.168.1.102:8080/api';
 

  register(User user)async{
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
      final SharedPreferences prefs = await _prefs;
var url = Uri.parse(baseUrl+'/users/register');
try {
  var client = http.Client();
  var response =await  client.post(url,
  body: {
    'name':user.name,
    'email':user.email,
    'phoneNumber':user.number,
    'password':user.password,
    'c_password':user.c_password
  },
  headers: {
    'Accept':'application/json'
  }
  );
if(response.statusCode==200){
var reg =await Registerstatus.fromJson(jsonDecode(response.body));
var token  =reg.data!.token;
print(token);
prefs.setString('token',token );
}
  
  else{
    print( 'Server Error');
  }
  
  
}
catch(e){
print(e);
}
}
login (User user )async{
final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
      final SharedPreferences prefs = await _prefs;
var url = Uri.parse(baseUrl+'/users/login');
var token = prefs.getString('token');
try {
  var client = http.Client();

var response =await  client.post(url,
  body: {
 
    'email':user.email,
  
    'password':user.password,

  },
  headers: {
    'Accept':'application/json',
    'Authorization': 'Bearer $token',
  }
  );
if(response.statusCode==200){
print('login Done !');
var loginStatus =await LoginStatus.fromJson(jsonDecode(response.body));
if (loginStatus.success){
  print('login Done');
  print(response.body);
  prefs.setString('name', loginStatus.data.name);
  print(prefs.getString('name'));
    prefs.setInt('id',127);
  print(prefs.getString('name'));
}
else{
  print('Error !');
}
}
  else{
    print( 'Server Error');
  }
  
  
}
catch(e){
print('this is '+e.toString());
}
}
Future<List<Trip>?>getEndTrips ()async{
var url = Uri.parse(baseUrl+'/endtrips');
final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
      final SharedPreferences prefs = await _prefs;
      var token = prefs.getString('token');
try {
  var client = http.Client();
  var response = await  client.get(url, headers: {
    'Accept':'application/json',
    'Authorization': 'Bearer $token',

  });
  if(response.statusCode==200){
    var data = await EndTrips.fromJson(jsonDecode(response.body)).trips;
    return data;
  }
  else{
    print('Server Error');
  }
}
catch(e){
print(e);
}
}
Future<Balance>GetBlanceApi()async{
  var url = Uri.parse(baseUrl+'/balance');
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
      final SharedPreferences prefs = await _prefs;
      var token = prefs.getString('token');
      try{
        var client = http.Client();
        var respose  = await client.get(url, headers: {
    'Accept':'application/json',
    'Authorization': 'Bearer $token',

  });
  if (respose.statusCode==200){
    return Balance.fromJson(jsonDecode(respose.body));
  }
  else{
    print('connection Error');
return Future.error('Server not available!!!');
  }
      }
      catch(e){
        print(e);
     return Future.error(e);
      }
}
  getGuides ()async{
   var url = Uri.parse(baseUrl+'/guide/index');
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
      final SharedPreferences prefs = await _prefs;
      var token = prefs.getString('token');
      try {
        var client = http.Client();
var response = await client.get(url,headers: {
    'Accept':'application/json',
    'Authorization': 'Bearer $token',

  });
  if (response.statusCode ==200){
return(jsonDecode(response.body)['data']);
    
  }
  else {
    print('server error');
    return Future.error('Server un available');
  }
      }catch(e){
        print(e);
return Future.error(e);
      }
}
Future <List<TripsObj>?> getAllTrips ()async{
 var url = Uri.parse(baseUrl+'/alltrips');
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
      final SharedPreferences prefs = await _prefs;
      var token = prefs.getString('token');
      try{
var client = http.Client();
var response =  await client.get(url, headers: {
    'Accept':'application/json',
    'Authorization': 'Bearer $token',

  });
  if(response.statusCode==200){
  return tripsObjFromJson(response.body);
  }
  else{
    return Future.error('Server Un Available !');
  }
      }catch(e){
        return Future.error(e);
      }
}
Future<List<Offer>?>getOfferts ()async{
 var url = Uri.parse(baseUrl+'/offers/index');
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
      final SharedPreferences prefs = await _prefs;
      var token = prefs.getString('token');
      try{
var client = http.Client();
var response =  await client.get(url, headers: {
    'Accept':'application/json',
    'Authorization': 'Bearer $token',

  });
  if(response.statusCode==200){
   var data = await offerFromJson(response.body);
    return data;
  }
  else{
    return Future.error('Server Un Available !');
  }
      }catch(e){
        return Future.error(e);
      }
}

Future<List<Datum>>?getOffertsUser ()async{
 var url = Uri.parse(baseUrl+'/Offers');
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
      final SharedPreferences prefs = await _prefs;
      var token = prefs.getString('token');
      try{
var client = http.Client();
var response =  await client.get(url, headers: {
    'Accept':'application/json',
    'Authorization': 'Bearer $token',

  });
  if(response.statusCode==200){
   var data = await OffertUser.fromJson(jsonDecode(response.body)).data;
    return data;
  }
  else{
    return Future.error('Server Un Available !');
  }
      }catch(e){
        return Future.error(e);
      }
}
getPlacesData(int type, int city )async{
  var url = Uri.parse(baseUrl+'/places/index?type_of_place_id=${type}&governorate_id=${city}');
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
      final SharedPreferences prefs = await _prefs;
      var token = prefs.getString('token');
      try {
        var client = http.Client();
var response = await client.get(url,


headers: {
    'Accept':'application/json',
    'Authorization': 'Bearer $token',

  });
  if (response.statusCode ==200){
print(jsonDecode(response.body)['places'][3][0]['name']);
    return jsonDecode(response.body)['places'];
  }
  else {
    print('server error');
    return Future.error('Server un available');
  }
      }catch(e){
        print(e);
return Future.error(e);
      }
}
deletePlace (id)async{
   var url = Uri.parse(baseUrl+'/places/delete?id=$id');
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
      final SharedPreferences prefs = await _prefs;
      var token = prefs.getString('token');
      try{
        var client = http.Client();
        var response= await client.delete(url ,
      headers: {
    'Accept':'application/json',
    'Authorization': 'Bearer $token',

  });
  if(response.statusCode==200){
    print( 'Place Deleteed Successfully !!');

  }
  else{
print('Err');
  }

      }
      catch(e){
        print(e);

      }
}
addNewResturantHotel (type , name , number , rate )async{
  var url = Uri.parse(baseUrl+'/places/add/hotels/rest?type_of_place_id=$type&governorate_id=1&name=$name&phoneNumber=$number&evaluation=$rate');
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
      final SharedPreferences prefs = await _prefs;
      var token = prefs.getString('token');
      try {
        var client = http.Client();
        var response = await client.post(url,
            headers: {
    'Accept':'application/json',
    'Authorization': 'Bearer $token',

  });
  if(response.statusCode==200){
    return 'Resturant / hotel Added Successfully !';
  }
  else{
    return Future.error('Server err');
  }
      }catch(e){
return Future.error(e);
      }

}
deleteOffer(id)async {
    var url = Uri.parse(baseUrl+'/offers/delete?id=$id');
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
      final SharedPreferences prefs = await _prefs;
      var token = prefs.getString('token');
      try {
        var client = http.Client();
var response = await client.delete(url,      headers: {
    'Accept':'application/json',
    'Authorization': 'Bearer $token',

  });
  if (response.statusCode==200 ){
    print('item Deleted Successfully  !');
  }
  else {
    return Future.error('Server Un Available');
  }
      }catch(e){
 return Future.error(e);
      }
}
updateOffer(id,name,starts_at,ends_at,percentage)async {
    var url = Uri.parse(baseUrl+'/offers/update?id=$id&name=$name&starts_at=$starts_at&ends_at=$ends_at&percentage=$percentage');
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
      final SharedPreferences prefs = await _prefs;
      var token = prefs.getString('token');
      try {
        var client = http.Client();
var response = await client.get(url,      headers: {
    'Accept':'application/json',
    'Authorization': 'Bearer $token',

  });
  if (response.statusCode==200 ){
    print('item Updated Successfully  !');
  }
  else {
    return Future.error('Server Un Available');
  }
      }catch(e){
 return Future.error(e);
      }
}
addNewOffer(name,starts_at,ends_at,percentage)async {
    var url = Uri.parse(baseUrl+'/offers/add?name=$name&starts_at=$starts_at&ends_at=$ends_at&percentage=$percentage');
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
      final SharedPreferences prefs = await _prefs;
      var token = prefs.getString('token');
      try {
        var client = http.Client();
var response = await client.post(url, headers: {
    'Accept':'application/json',
    'Authorization': 'Bearer $token',

  });
  if (response.statusCode==200 ){
    print('item added Successfully  !');
  }
  else {
    return Future.error('Server Un Available');
  }
      }catch(e){
 return Future.error(e);
      }
}
 getGovTrips(id)async{
    var url = Uri.parse(baseUrl+'/trip/gov?governorate_id=$id');
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
      final SharedPreferences prefs = await _prefs;
      var token = prefs.getString('token');
      try {
        var client = http.Client();
var response = await client.get(url,


headers: {
    'Accept':'application/json',
    'Authorization': 'Bearer $token',

  });
  if (response.statusCode ==200){
{
      print(jsonDecode(response.body));
return (jsonDecode(response.body));
}
    // return jsonDecode(response.body)['places'];
  }
  else {
    print('server error');
    return Future.error('Server un available');
  }
      }catch(e){
        print(e);
return Future.error(e);
      }
  }
  getPlaceDetails (id)async{
       var url = Uri.parse(baseUrl+'/places/details/$id');
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
      final SharedPreferences prefs = await _prefs;
      var token = prefs.getString('token');
      try {
        var client = http.Client();
        var response = await client.get(url,headers: {
    'Accept':'application/json',
    'Authorization': 'Bearer $token',

  });
        if (response.statusCode==200){
          return jsonDecode(response.body)[ "details_place"];
        
        }
        else{
          return Future.error('Server Un Available !');

        }
  }
  catch(e){
return Future.error(e);
  }

}
  Future<Trips>? getTripData(int id)async{
    print(id);
   var url = Uri.parse(baseUrl+'/trips/showtrips?trip_id=$id');
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
      final SharedPreferences prefs = await _prefs;
      var token = prefs.getString('token');

  try {
        var client = http.Client();
        var response = await client.get(url,headers: {
    'Accept':'application/json',
    'Authorization': 'Bearer $token',

  });
        if (response.statusCode==200){
          return Trips.fromJson((jsonDecode(response.body)));
        
        }
        else{
          return Future.error('Server Un Available !');

        }
  }
  catch(e){
return Future.error(e);
  }

}
getCarsData()async {
   var url = Uri.parse(baseUrl+'/cars');
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
      final SharedPreferences prefs = await _prefs;
      var token = prefs.getString('token');
      try {
        var client = http.Client();
var response = await client.get(url,


headers: {
    'Accept':'application/json',
    'Authorization': 'Bearer $token',

  });
  if (response.statusCode ==200){
{
      print(jsonDecode(response.body)['data']);
return (jsonDecode(response.body)['data']);
}
    // return jsonDecode(response.body)['places'];
  }
  else {
    print('server error');
    return Future.error('Server un available');
  }
      }catch(e){
        print(e);
return Future.error(e);
      }
}
bookTrip(tripId) async {
  var url = Uri.parse(baseUrl+'/add?user_id=15&trip_id-$tripId');
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
      final SharedPreferences prefs = await _prefs;
      var token = prefs.getString('token');
      try {
        var client = http.Client();
var response = await client.get(url,


headers: {
    'Accept':'application/json',
    'Authorization': 'Bearer $token',

  });
  if (response.statusCode ==200){
{
      print(jsonDecode(response.body)['data']);

}
    // return jsonDecode(response.body)['places'];
  }
  else {
    print('server error');
    return Future.error('Server un available');
  }
      }catch(e){
        print(e);
return Future.error(e);
      }

}
carTrip(carId) async {
  var url = Uri.parse(baseUrl+'/add?user_id=15&trip_id-$carId');
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
      final SharedPreferences prefs = await _prefs;
      var token = prefs.getString('token');
      try {
        var client = http.Client();
var response = await client.get(url,


headers: {
    'Accept':'application/json',
    'Authorization': 'Bearer $token',

  });
  if (response.statusCode ==200){
{
      print(jsonDecode(response.body)['data']);

}
    // return jsonDecode(response.body)['places'];
  }
  else {
    print('server error');
    return Future.error('Server un available');
  }
      }catch(e){
        print(e);
return Future.error(e);
      }

}
getUserTrips ()async {
   var url = Uri.parse(baseUrl+'/usersTrip?user_id=');
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
      final SharedPreferences prefs = await _prefs;
      var token = prefs.getString('token'); 
      try {
var client =http.Client();
var response = await client.get(url,headers: {
    'Accept':'application/json',
    'Authorization': 'Bearer $token',

  }); 
  if(response.statusCode==200){
    print(response.body);
  }
  else{
    return Future.error('Server Un Available');
  }
      }
      catch(e){
        return Future.error(e);
      }
}
}

