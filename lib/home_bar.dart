import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:udemy_flutter/balance_screen.dart';
import 'package:udemy_flutter/all_trip.dart';

import 'login_project.dart';
class HomeBar extends StatefulWidget {
  const HomeBar({Key? key}) : super(key: key);

  @override
  State<HomeBar> createState() => _HomeBarState();
}

class _HomeBarState extends State<HomeBar> {
  GlobalKey<ScaffoldState> _key = new GlobalKey<ScaffoldState>();
  final index = 0;
  final urlImages=[
    //'https://i.pinimg.com/236x/52/6e/14/526e144d98cede550d3500578bda8211.jpg',
    'https://static.hosteltur.com/app/public/uploads/img/articles/2013/09/01/M_5b14ea6fb7f86_shutterstock_HOTEL.jpg',
    'https://i.pinimg.com/564x/f4/e5/a5/f4e5a5fc974d3eb97df15ae47852fb77.jpg',
    'https://img.jakpost.net/c/2017/08/09/2017_08_09_30809_1502274587._large.jpg',
    // 'https://www.dubaidrivingcenter.net/images/offers/DDC-offers.jpg'
    'https://lp-cms-production.imgix.net/2020-11/500pxRF_82178129.jpg',
    'https://miuc.org/wp-content/uploads/2019/08/How-do-we-use-money.jpg',
    //'https://surrealhotels.com/wp-content/uploads/2014/10/Special_Offers2.jpg',
    'https://www.dubaidrivingcenter.net/images/offers/DDC-offers.jpg',
    'https://arlingtonva.s3.amazonaws.com/wp-content/uploads/sites/25/2013/12/restaurant.jpeg',
    'https://cf.ltkcdn.net/camping/images/orig/257248-1600x1030-group-camping-games-activities-adults.jpg',
    'https://lp-cms-production.s3.amazonaws.com/public/2021-07/shutterstockRF_1218546019.jpg',
  ];
  @override
  Widget build(BuildContext context) {
    Size _size = MediaQuery.of(context).size;
    return Scaffold(
      drawer: Drawer(
        child: TextButton(
          child: Text('Logout'),
          onPressed: ()async{
              final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
      final SharedPreferences prefs = await _prefs;
      prefs.setString('name', '');
              if(prefs.getString('name')==''){
                         Navigator.of(context).push(MaterialPageRoute(builder: (context) =>Test_Login()));
              }
        }),
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
          // Swiper(
          //       itemBuilder: (BuildContext context, int index)
          //       {
          //       final urlImage = urlImages[index];
          //       return ClipRRect(
          //         borderRadius: BorderRadius.only(
          //             bottomLeft: Radius.circular(50),
          //             bottomRight:Radius.circular(50)),
          //         child:Image.network(
          //           urlImage,
          //           fit: BoxFit.cover,),
          //       );
          //           },
          //             itemCount:urlImages.length,
          //             itemHeight:340,
          //             itemWidth:double.infinity,
          //             viewportFraction: 0.8,
          //             scale: 0.9,
          //             pagination: SwiperPagination(margin:EdgeInsets.only(top: 320)),
          //             layout: SwiperLayout.STACK,
          //            autoplay: true,
          //     ),
            Stack(
              children: [
                Container(
                  width: _size.width,
                  height: _size.height/2, // لل swiper
                  child: Swiper(
                    itemCount:9,
                    autoplay: false,
                    layout: SwiperLayout.CUSTOM,
                    customLayoutOption: CustomLayoutOption(startIndex: 0,stateCount: 3).addScale([0.7,0.85,1], Alignment.bottomCenter).addTranslate([
                      Offset(0, 30),
                      Offset(0, -10),

                      Offset(0, -44),
                      Offset(0, -100)
                    ]).addOpacity([
                      0.5,0.85,1,1
                    ]),
                    itemWidth: double.infinity,
                    loop: true,
                    itemBuilder: (BuildContext context,int index) {
                      final urlImage = urlImages[index];
                      return ClipRRect(
                      borderRadius: BorderRadius.only(bottomLeft: Radius.circular(50),
                          bottomRight: Radius.circular(50)),
                        child:Image.network(
                                     urlImage,
                              fit: BoxFit.cover,),
                    );
  }
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only( left:15.0,bottom: 4),
              child: Row(
                children: [
                  Text(
                    'Admin :',
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontFamily: 'OleoScriptSwashCaps',
                      fontSize: 40,
                      // color: Colors.white,
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10.0, right: 10.0,bottom: 10),
              child: ListView.separated(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemBuilder: (context, index) =>Column(
                    children: [
                      buildR(
                          content: 'All trips',
                          icon: Icons.arrow_forward_ios,
                          images: 'https://c8.alamy.com/comp/FP6XRY/travel-icon-internet-button-on-white-background-FP6XRY.jpg',
                      myFunc:(){ Navigator.of(context).push(
                          MaterialPageRoute(builder: (context) => AllTrip()));}),
                      buildR(
                          content: 'Balance',
                          icon: Icons.arrow_forward_ios,
                          images: 'https://toppng.com/uploads/preview/money-bag-icon-euros-11563611108asiwijaguc.png',
                      myFunc:(){  Navigator.of(context).push(
                          MaterialPageRoute(builder: (context) =>BalanceScreen()));} ),
                            buildR(
                          content: 'Logout ',
                          icon: Icons.arrow_forward_ios,
                          images: 'https://c8.alamy.com/comp/FP6XRY/travel-icon-internet-button-on-white-background-FP6XRY.jpg',
                      myFunc:()async{
                                final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
      final SharedPreferences prefs = await _prefs;
      prefs.setString('name', '');
              if(prefs.getString('name')==''){
                         Navigator.of(context).push(MaterialPageRoute(builder: (context) =>Test_Login()));
              }
                          
                          MaterialPageRoute(builder: (context) => AllTrip());}),

                    ],
                  ),
                  separatorBuilder: (context, index) =>
                      SizedBox(
                        height: 40.0,
                      ),
                  itemCount: 1),
            ),
        ],
      ),
    ),
      // body:Stack(
      //     alignment: Alignment.topCenter,
      //     children:
      //     [
      //       Image(
      //         image: NetworkImage('http://picsum.photos/500/500?random=4'),
      //         width:double.infinity,
      //         height: 300,
      //         fit: BoxFit.cover,
      //       ),
      //       Container(
      //         width: 269 ,
      //         color: Colors.black.withOpacity(.6),
      //         padding: EdgeInsets.symmetric(
      //           vertical: 10.0,
      //           //horizontal: 10.0,
      //         ),
      //         child: Text(
      //           'Flower',
      //           textAlign: TextAlign.center,
      //           style: TextStyle(
      //             fontSize: 20,
      //             color: Colors.white,
      //           ),
      //         ),
      //       ),
      //
      //       Padding(
      //         padding: const EdgeInsets.only(top:340),
      //             child: ListView.builder(
      //             itemCount: 7,
      //             itemBuilder: (context,index)=>Card(
      //               child: ListTile(
      //                 leading: CircleAvatar(radius:28 ,backgroundImage: NetworkImage('http://picsum.photos/500/500?random?sig=$index&woman'),),
      //                 title: Text('username $index'),
      //                 subtitle:Text('Email $index'),
      //                 trailing: Icon(Icons.arrow_forward_ios),
      //                 onTap: (){
      //                   Navigator.of(context).push(
      //                       MaterialPageRoute(builder:(context) =>SettingManeger()));
      //                 },
      //               ),
      //             ),
      //           ),
      //       ),
      //     ],
      // ),

      // ),
    );
  }

  //صور تحت بعض
  //Padding(
  //               padding: const EdgeInsets.all(14.0),
  //               child: ListView.separated(
  //                   physics: NeverScrollableScrollPhysics(),
  //                   shrinkWrap: true,
  //                   itemBuilder:(context, index) => bildChatItem(),
  //                   separatorBuilder: (context, index) =>SizedBox(
  //                     height: 20.0,
  //                   ),
  //                   itemCount: 10),
  //             ),
  //++
  // Widget bildChatItem() => Container(
  // color: Colors.deepPurpleAccent,
  // height: 400,
  // child: Container(
  // decoration: BoxDecoration(
  // image: DecorationImage(
  // image: NetworkImage('http://picsum.photos/500/500?random?sig=$index&natural'),
  // fit: BoxFit.cover,
  // )
  // ),
  // )
  // );
  // Widget bildChatItem() =>
  //     Card(
  //       shape: RoundedRectangleBorder(
  //           borderRadius: BorderRadius.circular(24)
  //       ),
  //       elevation: 8,
  //       shadowColor: Colors.black,
  //       child: ListTile(
  //         leading: CircleAvatar(radius: 28,
  //           backgroundImage: NetworkImage(
  //               'http://picsum.photos/500/500?random?&woman'),),
  //         title: Text('tttt $index ', style: TextStyle(
  //           fontWeight: FontWeight.w400,
  //           fontSize: 25,
  //         ),),
  //         subtitle: Text('tttt'),
  //         trailing: Icon(Icons.arrow_forward_ios),
  //         onTap: () {
  //           Navigator.of(context).push(
  //               MaterialPageRoute(builder: (context) => SettingManeger()));
  //         },
  //       ),
  //     );
  Widget buildR ({required String content,required IconData icon,required String images,required myFunc})=>
      Container(
        height: 75,
        child: Padding(
          padding: const EdgeInsets.all(2.0),
          child: Card(
           shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(34)
              ),
              color: Colors.grey[300],
              elevation: 10,
              shadowColor: Colors.black,
              child: ListTile(
                leading: Padding(
                  padding: const EdgeInsets.only(top: 8,left: 16),
                  child: CircleAvatar(radius: 22,
                    backgroundImage: NetworkImage(images),),
                ),
                title: Text(content, style: TextStyle(
                  fontWeight: FontWeight.w400,
                  fontFamily: 'Courgette',
                  fontSize: 30,
                ),),
                trailing: Icon(Icons.arrow_forward_ios),
               onTap: myFunc,
              ),
          ),
        ),
      );
}

