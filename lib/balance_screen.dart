import 'package:flutter/material.dart';
import 'package:udemy_flutter/objects/banalce.dart';
import 'package:udemy_flutter/services/http.dart';
class BalanceScreen extends StatefulWidget {
  const BalanceScreen({Key? key}) : super(key: key);

  @override
  State<BalanceScreen> createState() => _BalanceScreenState();
}

class _BalanceScreenState extends State<BalanceScreen> {
Balance? balance;
HttpService service = new HttpService();
bool isLoading = true;
int myBalace = -1; 
getBalance ()async{

balance =await service.GetBlanceApi();

if (balance!=null){
  setState(() {
    isLoading= false; 
    myBalace =balance!.data;
  });

}
}
@override
  void initState() {
    // TODO: implement initState
    super.initState();
    getBalance();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children:[
          ClipPath(
            clipper: CustomClipPath(),
            child: Container(
                color: Colors.white70,
                height: 400,
                child: Container(
                  decoration: BoxDecoration(
                      image: DecorationImage(
                        image: NetworkImage('https://www.studiekeuzelab.nl/hs-fs/hubfs/Afbeeldingen/Topics/%5BTopic%5D%20Header%20image/topic-money.jpg?width=625&name=topic-money.jpg'),
                        fit: BoxFit.cover,
                      )
                  ),
                )
            )
        ),
          Padding(
            padding: const EdgeInsets.only(right: 30,bottom: 40,top: 0),
            child: Text('  Balance',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontFamily: 'Courgette',
                fontSize: 45,
              ),
              textAlign: TextAlign.end,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 2.0, left: 10.0, right: 10.0),
            child: Visibility(
              visible: !isLoading,
              replacement: Center(child: CircularProgressIndicator()),
              child: ListView.separated(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemBuilder: (context, index) =>Column(
                    children: [
                      buildR(
                          content: 'Total balance : $myBalace',
                      )
                    ],
                  ),
                  separatorBuilder: (context, index) =>
                      SizedBox(
                        height: 40.0,
                      ),
                  itemCount: 1),
            ),
          ),
        ],
      ),
    );
  }
}
class  CustomClipPath extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    double w = size.width;
    double h = size.height;
    final path = Path();
    path.moveTo(0, 0);
    path.lineTo(0, size.height);
    path.quadraticBezierTo(
        size.width*0.39,
        size.height*0.98,
        size.width*0.70,
        size.height*0.80);
    path.quadraticBezierTo(
      size.width,
      size.height*0.64,
      size.width,
      size.height);
    path.lineTo(size.width, 0);
    path.close();
    return path;
  }
  @override
  bool shouldReclip(CustomClipper<Path> oldClipper){
    return true;
  }

}
Widget buildR ({required String content})=>
    Container(
      height: 140,
      child: Padding(
        padding: const EdgeInsets.only(bottom: 40),
        child: Card(
          color: Colors.teal.withOpacity(.8),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(34)
          ),
          elevation: 10,
          shadowColor: Colors.green.withOpacity(.1),
          child: ListTile(
            title: Padding(
              padding: const EdgeInsets.only(top: 28,left: 40),
              child: Text(content, style: TextStyle(
                fontWeight: FontWeight.w400,
                fontSize: 20,
                color: Colors.white,
              ),),
            ),
            onTap: (){},
          ),
        ),
      ),
    );
