import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:udemy_flutter/forget_password.dart';
import 'package:udemy_flutter/offer_user.dart';
import 'package:udemy_flutter/test_home_user.dart';
import 'package:udemy_flutter/home_admin.dart';
import 'package:udemy_flutter/register_screen.dart';
import 'package:udemy_flutter/services/http.dart';

import 'bottom_bar.dart';
import 'objects/user.dart';
class Test_Login extends StatefulWidget {
  const Test_Login({Key? key}) : super(key: key);

  @override
  State<Test_Login> createState() => _Test_LoginState();
}

class _Test_LoginState extends State<Test_Login> {

String emailErr= '';
String passErr = '';
  bool isPasswordVisible = false;
      

 

 
  var emailController = TextEditingController();
  var passwordController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/images/ر.jpg'),
         // image: NetworkImage('https://i.pinimg.com/originals/a0/99/2a/a0992afa9c1f2b47db51d11f69c897f3.jpg'),
          fit:BoxFit.cover,
          colorFilter: ColorFilter.mode(
            Colors.black.withOpacity(0.6),
            BlendMode.darken,
          ),
        ),
      ),
      child:Scaffold(
        backgroundColor: Colors.transparent,
        body:Center(
          child: SingleChildScrollView(
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 10,right: 10),
                  child: Text(
                    'Login',
                    style: TextStyle(
                      fontWeight: FontWeight.w200,
                      fontSize: 90.0,
                      color:Color(0xFF2AA8C0),
                      fontFamily: 'Courgette'
                    ),),
                ),
                SizedBox(
                  height: 20.0,
                ),
                Padding(padding: EdgeInsets.symmetric(vertical: 5.0),
                  child: Text(emailErr,style: TextStyle(color:  Colors.red),),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      left: 60,
                      right: 60,
                      bottom: 2,
                      top:8
                  ),
                  child: TextFormField(
                    controller: emailController,
                    keyboardType:TextInputType.emailAddress ,
                    style: TextStyle(
                      color: Colors.white,
                    ),
                    decoration: InputDecoration(
                      labelText: 'Email Address',
                      labelStyle: TextStyle(
                        color: Colors.white70,
                      ),
                      prefixIcon: Icon(
                        Icons.email,
                        color: Colors.white,),
                      border: OutlineInputBorder(),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white,width:1.6),
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                      focusedBorder:  OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white,width:1.6),
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                    ),
                  ),
                ),
                Padding(padding: EdgeInsets.symmetric(vertical: 5.0),
                  child: Text(passErr,style: TextStyle(color:  Colors.red),),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      left: 60,
                      right: 60,
                      top: 2,
                    bottom: 0
                  ),
                  child: TextFormField(
                    controller: passwordController,
                    keyboardType: TextInputType.visiblePassword,
                    style: TextStyle(
                      color: Colors.white,
                    ),
                    decoration: InputDecoration(
                      labelText:'Password',
                      labelStyle: TextStyle(
                        color: Colors.white70,
                      ),
                      border: OutlineInputBorder(),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white,width:1.6),
                          borderRadius: BorderRadius.circular(20.0)
                      ),
                      focusedBorder:  OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white,width:1.6),
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                      prefixIcon: Icon(Icons.lock,
                        color: Colors.white,),
                      suffixIcon: IconButton(
                        icon: isPasswordVisible?
                        Icon(Icons.visibility_off,
                          color: Colors.white,)
                            : Icon(Icons.visibility,
                          color: Colors.white,),
                        onPressed: () => setState(() => isPasswordVisible = !isPasswordVisible),
                      ),
                    ),
                    obscureText: isPasswordVisible,
                  ),
                ),
     
                tripBtn(content: 'Continue as a user',
                    icon:Icons.person,
                    color: Colors.transparent,
                    myFunc:()async{


                      if(emailController.text !=''&&passwordController.text!=''){
                      setState(() {
                        emailErr= '';
                        passErr='';
                      });
                      /*
                      *
                      * Login check is here
                      * */
           HttpService service = new HttpService();
              var user = new User(email: emailController.text 
                      
                      ,password: passwordController.text  , 
                   );
                      await service.login(user);
                               final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
      final SharedPreferences prefs = await _prefs;
      if(prefs.getString('name') != null){
  Navigator.of(context).
                        push(MaterialPageRoute(builder: (context)=>BottomBar(index:0)));
      }
                       
else{
  setState(() {
    emailErr ='Login Error';
  });
}
                      }
                      else if (emailController.text ==''){
                        setState(() {
                          emailErr ='Email Can not Be Empty !!';
                        });

                      }
                      else if (passwordController.text ==''){
                        setState(() {
                          passErr ='password Can not Be Empty !!';
                        });

                      }
                      else {
                        setState(() {
                          emailErr = 'please Fill Data';
                        });
                      }
                       }),
                // TextButton(
                //
                //   onPressed: (){Navigator.of(context).
                //   push(MaterialPageRoute(builder: (context)=>Me_Screen()));},
                //   child: Row(
                //     mainAxisAlignment: MainAxisAlignment.center,
                //     children: [
                //       Icon(Icons.person,
                //         color: Color(0xFFFFE6C7),
                //         size: 35,),
                //       Text('Continue as a user',
                //         style: TextStyle(
                //           color: Colors.white,
                //           fontWeight: FontWeight.bold,
                //           fontSize: 30,
                //         ),
                //       ),
                //     ],
                //   ),
                // ),
                // Container(
                //   width:200 ,
                //   //color: Colors.blue,
                //   child: MaterialButton(onPressed: (){
                //     print(emailController.text);
                //     print(passwordController.text);
                //   },
                //     color: Colors.deepPurple,
                //     child: Text(
                //       'LOGIN',
                //       style: TextStyle(
                //         color: Colors.white,
                //       ),
                //     ),
                //   ),
                // ),
                // TextButton(
                //   onPressed: (){Navigator.of(context).
                //   push(MaterialPageRoute(builder: (context)=>Maneger2Screen()));},
                //   child: Row(
                //     children: [
                //       Icon(Icons.manage_accounts,
                //       color: Color(0xFFFFE6C7),
                //       size: 30,),
                //       Text('Admin',
                //         style: TextStyle(
                //           color: Colors.white,
                //           fontWeight: FontWeight.bold,
                //           fontSize: 20,
                //         ),
                //       ),
                //     ],
                //   ),
                // ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children:
                  [ Icon(Icons.manage_accounts,
                          color: Color(0xFFFFE6C7),
                          size: 30,),
                    Text(
                      'Log in as ',
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                          fontSize: 18
                      ),
                    ),
                    TextButton(
                      onPressed: ()async{
if (emailController.text=='admin'&&passwordController.text=='admin'){
  Navigator.of(context).
                      push(MaterialPageRoute(builder: (context)=>HomeAdmin()));
                       final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
      final SharedPreferences prefs = await _prefs;
      prefs.setString('admin', 'admin');
}
else{
  setState(() {
    emailErr= 'Admin Login Error';
  });
}

                      },
                      child: Text('Admin',
                        style: TextStyle(
                            color: Color(0xFF2AA8C0),
                            fontWeight: FontWeight.bold,
                            fontSize: 16
                        ),
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children:
                  [
                    Text(
                        'Don\'t have an account',
                      style: TextStyle(
                        color: Colors.white,
                          fontWeight: FontWeight.w600,
                          fontSize: 16
                      ),
                    ),
                    TextButton(
                      onPressed: (){Navigator.of(context).
                      push(MaterialPageRoute(builder: (context)=>RegisterScreen()));},
                      child: Text('Register now',
                        style: TextStyle(
                            color: Color(0xFF2AA8C0),
                          fontWeight: FontWeight.bold,
                          fontSize: 16
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),

    );
  }
  RaisedButton tripBtn(
      {required String content,required IconData icon,required Color color,required myFunc})
  {
    return RaisedButton(color: color,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
      child: Container(
        width: 300,
        height: 60,
        padding: EdgeInsets.symmetric(horizontal: 10,vertical: 5),
        child: Row(
          children: [
            Icon(
              icon,
              size: 20,
              color: Colors.white,
            ),
            SizedBox(
              width: 20,
            ),
            Text(
              content,
              style: TextStyle(
                  color:Colors.white ,
                  fontSize: 26),
            ),
          ],
        ),
      ),
      onPressed: myFunc,
    );
  }
}