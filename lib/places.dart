
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:udemy_flutter/archa.dart';
import 'package:udemy_flutter/religious.dart';

import 'natural.dart';

class TourismPlaces extends StatefulWidget {
  const TourismPlaces({ Key? key }) : super(key: key);

  @override
  State<TourismPlaces> createState() => _TourismPlaceState();
}

class _TourismPlaceState extends State<TourismPlaces> {
  int index=0;
  @override
  
  Widget build(BuildContext context) {
     return Container(
      decoration: BoxDecoration(image: DecorationImage(image: NetworkImage('https://picsum.photos/500/500?random=4')
      ,fit: BoxFit.cover,
      colorFilter: ColorFilter.mode(Colors.black.withOpacity(0.5), BlendMode.darken))),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            leading: IconButton(icon:Icon(CupertinoIcons.back,size: 35,color: Colors.white,),onPressed: (){
              Navigator.of(context).pop();
            },),
            actions: [
              IconButton(
               onPressed: (){
             showDialog(context: context, builder: (BuildContext context){
                  return AlertDialog(
                    title: Text('This interface displays the types of places that can be visited, as it displays natural, archaeological and religious places.',
                    style: TextStyle(fontSize:25,fontWeight: FontWeight.bold,color:Colors.teal  ),),
                    
                    );

                });


          },
              icon: Icon(CupertinoIcons.question_circle,color: Colors.white,size: 30,))
            
          ],),
          
          //العنوان

       body: ListView(
         children: [
           Center(child: Text(".. Tourism Places ..",
           style: TextStyle(fontWeight: FontWeight.bold,fontSize: 40,color: Colors.white),),)
         ,
         SizedBox(height: 50,),
         
         Padding(padding:
    const EdgeInsets.symmetric(horizontal: 20,),
             child: ListView.separated(
               physics: NeverScrollableScrollPhysics(),
               shrinkWrap: true,
               itemBuilder: (context,index)=>Column(
                 children: [
                   bild(content: 'Natural Places',icon:Icons.arrow_forward_ios ,images:'https://picsum.photos/500/500?random=1',
                   myff:() { Navigator.of(context).push(MaterialPageRoute(builder: (context)=>NaturalPage()));}  ),
                 bild(content: 'Religious Places',icon:Icons.arrow_forward_ios,images:'https://picsum.photos/500/500?random=2',
                     myff:() { Navigator.of(context).push(MaterialPageRoute(builder: (context)=>RelagousPage()));}),
                 bild(content: 'Archaeological Places',icon:Icons.arrow_forward_ios,images:'https://picsum.photos/500/500?random=3',
                   myff:() { Navigator.of(context).push(MaterialPageRoute(builder: (context)=>ArchaPage()));}),
                 ],
               ),
              separatorBuilder: (context,index)=>
              SizedBox(height: 8.0,)
              , itemCount:1),

         ),
        

         ]

       ),
       ),
       );
      
    
  }
  Widget bild({required String content ,required IconData icon,required String images,required myff})=> Padding(
    padding: const EdgeInsets.symmetric(vertical: 17),
    child: Card(
  
      child: ListTile(leading: CircleAvatar(radius: 27,backgroundImage:NetworkImage(images) ,
      ),title: Text(content,style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
      trailing: Icon(icon),
      onTap: myff,

      ),
      
      ),
  );
  
}