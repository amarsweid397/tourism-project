import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:udemy_flutter/balance_screen.dart';
import 'package:udemy_flutter/bottom_bar.dart';
import 'package:udemy_flutter/carbooking.dart';
import 'package:udemy_flutter/edit_places.dart';
import 'package:udemy_flutter/editing_screen.dart';
import 'package:udemy_flutter/group_trip.dart';
import 'package:udemy_flutter/home_bar.dart';
import 'package:udemy_flutter/hotel.dart';
import 'package:udemy_flutter/login_project.dart';
import 'package:udemy_flutter/activity_admin.dart';
import 'package:udemy_flutter/home_admin.dart';
import 'package:udemy_flutter/objects/end_trips.dart';
import 'package:udemy_flutter/offer_user.dart';
import 'package:udemy_flutter/offers_admin.dart';
import 'package:udemy_flutter/places_screen.dart';
import 'package:udemy_flutter/register_screen.dart';
import 'package:udemy_flutter/all_trip.dart';
import 'package:udemy_flutter/resturant.dart';
import 'package:udemy_flutter/trip.dart';
import 'package:udemy_flutter/trips_made.dart';
import 'package:udemy_flutter/view_pro/swiper.dart';

import 'test_home_user.dart';


void main()  {
 runApp(MyApp());

}

class MyApp extends StatefulWidget {
  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
    String Token = '';
    String Name = '';
        String Admin = '';
    bool isFirstTime = true ; 

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getToken();
getName();
checkFirstTime();
  }

  getToken()async{
         final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
      final SharedPreferences prefs = await _prefs;
      prefs.remove('name');
      
    

  
   Token = await  prefs.getString('token')??'';
   Admin =await  prefs.getString('admin')??'';
  }


  getName()async{
         final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
      final SharedPreferences prefs = await _prefs;
     Name = await  prefs.getString('name')??'';

  }
 checkFirstTime()async{
         final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
      final SharedPreferences prefs = await _prefs;
    

  
  setState(() {
     isFirstTime =   prefs.getBool('first')??true;
  });
   print(isFirstTime);
  }

  @override
  Widget build(BuildContext context) {
    final navigatorKey = GlobalKey<NavigatorState>();

    const bodyTextStyle= TextStyle(color: Colors.white,fontSize: 24,fontWeight: FontWeight.w600);
   const titleTextStyle =TextStyle(color: Colors.white,fontSize: 30 ,fontWeight: FontWeight.bold);
    void _onIntroEnd(BuildContext context)async {
        final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
      final SharedPreferences prefs = await _prefs;
     await prefs.setBool('first', false);
      
      navigatorKey.currentState!.push(MaterialPageRoute(builder: (_)=>Token!=''&&Name==''?Test_Login():Token!=''&&Name!=''?TestHomeUser():  RegisterScreen()));}
    return MaterialApp(
      navigatorKey: navigatorKey,
        debugShowCheckedModeBanner: false,
        home:isFirstTime?
         IntroductionScreen(
      globalFooter: null,
          globalHeader: Align(
            alignment: Alignment.topRight,
            child: SafeArea(
              child: Padding(
                padding: const EdgeInsets.only(top: 16, right: 16),

              ),
            ),
          ),

          pages: [
            PageViewModel(
              footer:null,
              decoration: PageDecoration(
                titleTextStyle: titleTextStyle,
                bodyTextStyle: bodyTextStyle,
                boxDecoration: BoxDecoration(image: DecorationImage(image: AssetImage('assets/images/ph.jpg'),fit: BoxFit.fill,),)
              ),
              title: "Fractional shares",
              body:
              "Instead of having to buy an entire share, invest any amount you want.",
            ),
            PageViewModel(

              decoration: PageDecoration(
                  titleTextStyle: titleTextStyle,
                  bodyTextStyle: bodyTextStyle,
                  boxDecoration: BoxDecoration(image: DecorationImage(image: AssetImage('assets/images/He.jpg'),fit: BoxFit.fill))
              ),
              title: "Learn as you go",
              body:
              "Download the Stockpile app and master the market with our mini-lesson.",
            ),
            PageViewModel(

              decoration: PageDecoration(
                  titleTextStyle: titleTextStyle,
                  bodyTextStyle: bodyTextStyle,
                  boxDecoration: BoxDecoration(image: DecorationImage(image: AssetImage('assets/images/6.jpg'),fit: BoxFit.cover))
              ),
              title: "Kids and teens",
              body:
              "Kids and teens can track their stocks 24/7 and place trades that you approve.",
            ),
            PageViewModel(
              decoration: PageDecoration(
                  titleTextStyle: titleTextStyle,
                  bodyTextStyle: bodyTextStyle,
                  boxDecoration: BoxDecoration(image: DecorationImage(image: AssetImage('assets/images/4.jpg'),fit: BoxFit.cover))
              ),
              title: "let\'s get started ",
              body:
              ".",
            ),
          ],
          onDone: ()=>_onIntroEnd(context)
       ,   //onSkip: () => _onIntroEnd(context), // You can override onSkip callback
          showSkipButton: false,
          skipOrBackFlex: 0,
          nextFlex: 0,
          showBackButton: true,
          //rtl: true, // Display as right-to-left
          back: const Icon(Icons.arrow_back),
          skip: const Text('Skip', style: TextStyle(fontWeight: FontWeight.w600)),
          next: const Icon(Icons.arrow_forward),
          done: const Text('Done', style: TextStyle(fontWeight: FontWeight.w600,
          fontSize: 30
          )),
          curve: Curves.fastLinearToSlowEaseIn,
          controlsMargin: const EdgeInsets.all(16),

          // dotsDecorator: const DotsDecorator(
          //   size: Size(10.0, 10.0),
          //   color: Color(0xFFBDBDBD),
          //   activeSize: Size(22.0, 10.0),
          //   activeShape: RoundedRectangleBorder(
          //     borderRadius: BorderRadius.all(Radius.circular(25.0)),
          //   ),
          // ),

          dotsContainerDecorator:  ShapeDecoration(
             color:Color(0xFFFFE6C7),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
          ))):Token!=''&&Name==''?Test_Login():Token!=''&&Name!=''?BottomBar(index:0):Token!=''&&Name!=''&&Admin!=''?HomeAdmin() : RegisterScreen(),

       // home: HomeBar(),
      //   //home: LoginScreen(),
      //   //home:HomeScreen() ,

     );
  }
}
