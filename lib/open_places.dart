import 'package:flutter/material.dart';
class OpenPlaces extends StatefulWidget {
  final int id ;
  final String img ;

  const OpenPlaces({Key? key, required this.id, required this.img}) : super(key: key); 
  @override
  State<OpenPlaces> createState() => _OpenPlacesState();
}
class _OpenPlacesState extends State<OpenPlaces> {
  @override
  Widget build(BuildContext context) {
    Size _size = MediaQuery.of(context).size;
    return Scaffold(
      body: ListView(
      scrollDirection:Axis.vertical,
      children: [
        Stack(children: [Container(
        width:_size.width ,
          height: _size.height/2.4,
          decoration: BoxDecoration( borderRadius: BorderRadius.only(bottomLeft: Radius.circular(50),
              bottomRight: Radius.circular(50))
              , image:DecorationImage(
                  image: NetworkImage('https://cf.bstatic.com/xdata/images/hotel/max1280x900/180669765.jpg?k=cb935e74a3110efdcd29c1bcbe614cd564d3fc300f35453fbd909122a5fa2e1a&o=&hp=1')
                  ,fit:BoxFit.cover )),
        ),
        ],),
        Padding(padding: const EdgeInsets.all(8.3),
          child: Column(
            children: [
              SizedBox(height: 10,),
              Positioned(
                  top: 10,right: 10,
                  child:Column(children: [
                    Row(children:[
                      Icon(Icons.sort_by_alpha,color: Colors.blue,),
                      SizedBox(width: 5,)
                      ,Text('General information :',style:TextStyle(fontSize: 25,fontWeight: FontWeight.bold,color: Colors.black))
                    ])
                  ],)),
              SizedBox(height: 20,),
              Text('It is one of the oldest, most beautiful and most visited places. It contains antiquities that are more than a hundred years old'
              ,style: TextStyle(fontSize: 20,fontWeight:FontWeight.bold),),
               SizedBox(height: 20,),
               Center( 
                child: TextButton(
                onPressed:(){Navigator.of(context).pop();} ,
                 child: Row(        
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(Icons.arrow_left),
                    SizedBox(width: 6,),
                    Text('Back',
                    style: TextStyle(fontSize: 20,fontWeight:FontWeight.bold),)

                  ],
                 )))
            ],
          ),
        ),
    ]
    ),
    );
  }
}
