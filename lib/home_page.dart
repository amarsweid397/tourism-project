
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:udemy_flutter/login_project.dart';
import 'package:udemy_flutter/single_trip.dart';
import 'package:udemy_flutter/sittings.dart'; 
import './sources/lang.dart';
import 'account.dart';
import 'bottom_bar.dart';
import 'language.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
   
   int lang = 0 ;
          final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

 Future <void > getLang () async {
 
var x = await _prefs.then((SharedPreferences prefs) => prefs.getInt('Lang')??0 ) ;
print('lang is $lang');
setState(() {
  lang = x;
});

 }
 void initState() {
    super.initState();
getLang();

  }
GlobalKey<ScaffoldState> _key = new GlobalKey<ScaffoldState>(); // من اجل ال  drawer
  @override
  Widget build(BuildContext context) {

    Size _size = MediaQuery.of(context).size;
    return Scaffold(
     
       key: _key,
       body: ListView(
         
          scrollDirection: Axis.vertical,
          children: [
            Stack(
              children: [
               Container(
                width: _size.width,
                height: _size.height/2, // لل swiper
                child: Swiper(

                        itemCount:6,
                        //autoplay: true,
                        layout: SwiperLayout.CUSTOM,
                        customLayoutOption: CustomLayoutOption(startIndex: 0,stateCount: 3).addScale([0.7,0.85,1], Alignment.bottomCenter).addTranslate([
                          Offset(0, 40),
                          Offset(0, -10),

                          Offset(0, -50),
                          Offset(0, -100)
                        ]).addOpacity([
                          0.5,0.85,1,1
                        ]),
                        itemWidth: double.infinity,
                        loop: true,
                        itemBuilder: (BuildContext context,int index) => ClipRRect(
                          borderRadius: BorderRadius.only(bottomLeft: Radius.circular(50),
                          bottomRight: Radius.circular(50)),
                          child: Container(
                        decoration: BoxDecoration( image:DecorationImage(image: 
                        AssetImage('assets/images/p${index+1}.jpg'),
                            fit: BoxFit.fill),),
                          ),
                        ),
                      ),
              ),
            
              
              // للنص بال swiper
              Positioned(
                    top: 45,
                    left: 15,
                   // child: Title(
                        //color: Colors.black,
                        child: 
                        Container(
                          width:MediaQuery.of(context).size.width,
                          padding: EdgeInsets.only(right: 20),
                          child: 
                        Text(lang==0 ? '${english["welcome"]}':'${arabic["welcome"]}',
                        textAlign:lang==0? TextAlign.left: TextAlign.right,
                            style: TextStyle(
                                 fontFamily: 'water',
                                fontWeight: FontWeight.bold,
                                fontSize: 30,
                                color: Colors.white)),)
                        ),
            // drawer ايقونة ال
              Positioned(
                top: 10,
                left: lang==0?0:null,
                right: lang==1?0:null,
                child: Padding(
                padding: const EdgeInsets.only(top: 0,left: 5),
                //child: Container(
                  child: Row(children: [IconButton(onPressed: (){
                  _key.currentState!.openDrawer();
                }, icon:Icon(Icons.menu,size:30,color: Colors.white,) )],),),
              ),
            
              ],
            ),

            

            // النص الاول
            Padding(
              padding: const EdgeInsets.only( left:15.0,
            right: 20
              ),

              child: Container(
                child: 
                  Text(
lang==0 ? '${english["my_trips"]}':'${arabic["my_trips"]}',
                        textAlign:lang==0? TextAlign.left: TextAlign.right,
                    style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 30,
                   // color: Colors.white,
                    ),
                  ),
                
              ),
            ),

            SizedBox(
              height: 10,
            ),
            // الازرار
            Container(
              padding: EdgeInsets.symmetric(horizontal: 15), //
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  tripBtn(color:Colors.deepPurpleAccent,myFunc: ()async{
                  Navigator.of(context).push(MaterialPageRoute(builder: (context)=>SingleTripPage()));

                  },content: 'Single Trip'
                  ,icon:Icons.person
                  ),
                  tripBtn(color:Colors.red,myFunc: (){
                    Navigator.of(context).push(MaterialPageRoute(builder: (context) =>BottomBar(index: 4),));
                  },content: 'Group Trip'
                  ,icon:Icons.group
                  )
                ],
              ),
            ),
            
             // النص الثاني
             Padding(
               padding: const EdgeInsets.only(left: 15,top: 15),
               child: Text(
                      'Pupular Destinations :',
                  
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 30, //  color: Colors.white,
                      ),),
             )
         ,

       Container(
         padding: EdgeInsets.symmetric(vertical: 15),
         child: SingleChildScrollView(
           scrollDirection: Axis.horizontal,
           child: Row(
             children: [
             PopularWidget(
               name:'Raas Al Basset ',
               size: _size,imageSrc:'assets/images/pp1.jpg',rating: '5.7',),
             PopularWidget(
               name:'Ummayad Mosque ',
               size: _size,imageSrc: 'assets/images/pp2.jpg',rating: '6.3',),
             PopularWidget(
               name:'Aleepo  Castle ',
               size: _size,imageSrc:'assets/images/pp4.jpg',rating: '8.1',),       
             PopularWidget(
               name:'Nile Valley ',
               size: _size,imageSrc:'assets/images/pp3.jpg',rating: '3.7',),
        
        
           ],),
         ),
       )
          ],

        ), 

         // Drawer
         drawer:
            Drawer(
              child: ListView(
                padding: EdgeInsets.only(top: 40),
            scrollDirection: Axis.vertical,
             children: [
               Container(
                padding: EdgeInsets.symmetric(vertical: 30), //
                child: Column(
                  children: [
                       Padding(
                      padding: const EdgeInsets.only(bottom:40,left:25,right: 30  ),
                      child: Container(
            width:_size.width/2.0 ,
            height: _size.height/5.6,
          decoration: BoxDecoration( borderRadius: BorderRadius.circular(150)
         , image:DecorationImage(image: NetworkImage('https://picsum.photos/500/500?random=2')
          ,fit:BoxFit.fill )),
          ),
                    ),

            Text(
              " Welcome To \n.. Tourism in syria ..",
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 33, fontWeight: FontWeight.bold, color:Colors.teal),
            ),
            SizedBox(height: 30,),

            

                    tripBtn(
                      padding: true,
                      color:Color.fromRGBO(71,53 , 79, 0.5),myFunc: (){
                    Navigator.of(context).push(MaterialPageRoute(builder: (context)=>languagePage()));
                    },content: 'Language'
                    ,icon:Icons.language
                    ),
                    SizedBox(height:30 ,),
                    tripBtn(
                       padding: true,
                         color:Colors.teal,myFunc: (){
                      Navigator.of(context).push(MaterialPageRoute(builder: (context) =>AccountPage(),));
                    },content: 'My Account'
                    ,icon:Icons.account_balance_sharp
                    ), SizedBox(height:30 ,),
                      
                     tripBtn(
                       padding: true,
                       color:Color.fromRGBO(71,53 , 79, 0.5),myFunc: ()async{
      final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
      final SharedPreferences prefs = await _prefs;
      prefs.setString('name', '');
              if(prefs.getString('name')==''){
                         Navigator.of(context).push(MaterialPageRoute(builder: (context) =>Test_Login()));
              }
                    },content: 'Log out'
                    ,icon:Icons.logout
                    ),
                    SizedBox(height:30 ,),
                    tripBtn(
                      padding: true,
                      color:Colors.teal,myFunc: (){
                        
                showDialog(context: context, builder: (BuildContext context){
                  return AlertDialog(
                    title: Text('In this application, we talk about the scene in Syria, and it will display individual and group trips and book online or through the company.'
                    ,style: TextStyle(fontSize:20,fontWeight: FontWeight.bold,color:Colors.teal  ),),
                   
                    
                    );

                });
                     
                    },content: 'Help !'
                    ,icon:Icons.help
                    ), SizedBox(height:40 ,),

                 
                   
                  ],
                ),


             ),
              
             ],
             

             

             ),
            ),
        
        
    );
        
  }



  Widget tripBtn({required String content , required IconData icon ,required Color color,required  myFunc,bool padding=false}) {
    return 
    padding?Padding(padding: EdgeInsets.symmetric(horizontal: 20),child: tripBtn(content: content, icon: icon, color: color, myFunc: myFunc),):
    RaisedButton(

                    color: color,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                      child: Row(
                        children: [
                          Icon(
                            icon,
                            size: 32,
                            color: Colors.white,
                          ),
                          SizedBox(width:5,),
                          Text(content,
                              style: TextStyle(
                                  color: Colors.white, fontSize: 22))
                        ],
                      ),
                    ),
                    onPressed:myFunc);
  }
}


   PopularWidget({ required Size size, required String imageSrc,required String rating,required String name})
   {
    return Stack(
      children: [
        Container(
          margin: EdgeInsets.symmetric(horizontal: 20),
          width:size.width*0.45,height: 200,
          decoration: BoxDecoration(color: Colors.red,borderRadius: BorderRadius.circular(20)
         , image:DecorationImage(image: AssetImage(imageSrc,),fit: BoxFit.cover,
          ),
            ),
          ),

          Positioned(
            top: 20,left: 30,
            child:Column(children: [
            Text(name,style:TextStyle(fontSize: 22,fontWeight: FontWeight.bold,color: Colors.white))
            ,Row(children:[
   Icon(CupertinoIcons.star_fill,color: Colors.yellow,),SizedBox(width: 5,)
  ,Text(rating,style:TextStyle(fontSize: 23,fontWeight: FontWeight.bold,color: Colors.white))
            ])
          ],))
      ],
    );
  
}

// get showDialog{
//  GestureDetector(child:Icon(Icons.share) ,
//  onTap:() {
//    showDialog(context: context, builder:(BuildContext build){return AlertDialog(
//      content:Container(width: 30,height: 40,child:Text('Hello') ),
//  );} );
//  } ,);
// }