import 'package:flutter/material.dart';
import 'package:udemy_flutter/reseet_password.dart';
class ForgetPassword extends StatelessWidget {
  const ForgetPassword({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
       body: Container(
        decoration: BoxDecoration(
        image: DecorationImage(
        image: NetworkImage('https://t3.ftcdn.net/jpg/01/91/78/32/360_F_191783282_0TVrx5VrvrkpDHSKdjjI87HkbXJy5TMw.jpg'),
    fit:BoxFit.cover,
    colorFilter: ColorFilter.mode(
      Colors.black.withOpacity(0.4),
      BlendMode.darken,
    ),
    ),
    ),
        child:Scaffold(
          backgroundColor: Colors.transparent,
          body:Center(
            child: SingleChildScrollView(
               child: Column(
                 children: [
                 Image(
                image: NetworkImage('https://icon-library.com/images/reset-password-icon/reset-password-icon-29.jpg',
                     ),width: 190,
                   colorBlendMode: BlendMode.clear,
                 ),
                  SizedBox(
                    height: 10,
                  ),
                  ListTile(
                  title: Text(
                    'Forgot Password?',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 40.0,
                      color: Colors.white,
                      fontFamily: 'Courgette'
                    ),
                  textAlign: TextAlign.center,),
                  subtitle: Text('    Please enter your phone number to reset your password',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18.0,
                      color: Colors.white,),
                    textAlign: TextAlign.center,
                      ),
                ),
                  SizedBox(
                  height: 10.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                    left: 60,
                    right: 60,
                    top:30,
                      bottom: 50,
                    ),
                    child: TextFormField(
                    keyboardType:TextInputType.phone ,
                    style: TextStyle(
                    color: Colors.white,
                    ),
                      decoration: InputDecoration(
                      labelText: 'phone number',
                      labelStyle: TextStyle(
                      color: Colors.white70,
                      ),
                      prefixIcon: Icon(
                      Icons.phone,
                      color: Colors.white,),
                      border: OutlineInputBorder(),
                      enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.white,width:1.6),
                      borderRadius: BorderRadius.circular(20.0),
                      ),
                      focusedBorder:  OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.white,width:1.6),
                      borderRadius: BorderRadius.circular(20.0),
            ),
            ),
            ),
            ),
                   Row(
                     mainAxisAlignment: MainAxisAlignment.end,
                     children: [
                       Padding(
                         padding: const EdgeInsets.only(top: 10,
                         right: 20,
                         bottom: 40),
                         child: tripBtn(
                             content: 'Done',
                             icon: Icons.check_circle,
                             color: Colors.blue.withOpacity(.4),
                             myFunc: (){
                               Navigator.of(context).
                               push(MaterialPageRoute(builder: (context)=>ResetPassword()));
                             }),
                       ),
                     ],
                   ),

            ]
            )
            )
              )
            )
            )
              );
  }
  RaisedButton tripBtn(
      {required String content,required IconData icon,required Color color,required myFunc})
  {
    return RaisedButton(color: color,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
      child: Container(
        width: 120,
        height: 40,
        padding: EdgeInsets.symmetric(horizontal: 10,vertical: 5),
        child: Row(
          children: [
            Icon(
              icon,
              size: 28,
              color: Colors.white,
            ),
            SizedBox(
              width: 10,
            ),
            Text(
              content,
              style: TextStyle(
                  color:Colors.white ,
                  fontSize: 26),
            ),
          ],
        ),
      ),
      onPressed: myFunc,
    );
  }
}
