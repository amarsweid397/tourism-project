
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Trips extends StatefulWidget {
  const Trips({ Key? key }) : super(key: key);

  @override
  State<Trips> createState() => _TripsState();
}

class _TripsState extends State<Trips> {
  int index=0;
  @override

  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(image: DecorationImage(image: NetworkImage('https://i.pinimg.com/236x/df/ce/70/dfce708dacf95c0134f8cbfa2919f5e0.jpg')
          ,fit: BoxFit.cover,
          colorFilter: ColorFilter.mode(Colors.black.withOpacity(0.2), BlendMode.darken)
      )
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          leading: IconButton(icon:Icon(CupertinoIcons.back,size: 35,color: Colors.white,),onPressed: (){
            Navigator.of(context).pop();
          },),
        ),

        //العنوان

        body: ListView(
            children: [
              Center(child: Text(".. Licensed trips..",
                style: TextStyle(fontWeight: FontWeight.bold,fontSize: 40,color: Colors.white,fontFamily: 'Courgette'),),)
              ,
              SizedBox(height: 50,),

              Padding(padding:
              const EdgeInsets.symmetric(horizontal: 10,),
                child: ListView.separated(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemBuilder: (context,index)=>Column(
                      children: [
                        bild(content: 'Eid Trip',),
                        bild(content: 'Nice Trip',),
                        bild(content: 'Funny Trip',),
                        bild(content: 'Summer Trip',),
                      ],
                    ),
                    separatorBuilder: (context,index)=>
                        SizedBox(height: 4,)
                    , itemCount:1),

              ),
              SizedBox(height: 20,),
              Padding(
                padding: const EdgeInsets.only(left: 230),
                child: FloatingActionButton(onPressed: (){},child: Icon(Icons.add,color: Colors.blue),backgroundColor: Colors.white),
              )


            ]

        ),
      ),
    );


  }
  Widget bild({required String content})=> Padding(
    padding: const EdgeInsets.symmetric(vertical: 8),
    child: Container(
      child: Card(
        color: Colors.white,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15)
        ),
        elevation: 10,
        shadowColor: Colors.blue,
        child: ListTile(
          leading: CircleAvatar(
            backgroundColor: Colors.white38,
            radius: 18,backgroundImage:NetworkImage('https://static.thenounproject.com/png/2548936-200.png') ,
          ),title: Row(
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text(content,
                      style: TextStyle(fontSize: 25,fontWeight: FontWeight.bold,color: Colors.blue),),
                    SizedBox(width: 6,),
                  ],
                ),
                Text(
                  'It includes several different areas',
                  style: TextStyle(fontSize: 14,fontWeight: FontWeight.bold),),
              ],
            ),
            SizedBox(width: 8,),
            Icon(Icons.delete,color: Colors.red,)
          ],
        ),

        ),

      ),
    ),
  );

}
