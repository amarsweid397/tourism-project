import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:udemy_flutter/login_project.dart';
class FirsrtScreen extends StatefulWidget {
  const FirsrtScreen({Key? key}) : super(key: key);

  @override
  State<FirsrtScreen> createState() => _FirsrtScreenState();
}

class _FirsrtScreenState extends State<FirsrtScreen> {
  @override
  var index;
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: [
          Image.asset('assets/images/ph.jpg', fit: BoxFit.cover,),
          SafeArea(child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 30),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Let\'s make',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 35,
                  wordSpacing: 2.5,
                ),),
                Text('your dream\nvacation.',
                  style: TextStyle(
                    wordSpacing: 4.5,
                      color: Colors.white,
                      fontSize: 38,
                    fontWeight: FontWeight.bold
                  ),),
                SizedBox(
                  height: 380,
                ),
                Container(
                  height: 60,
                  width: 2000,
                   decoration: BoxDecoration(
                       color: Color(0xFF2AA8C0),
                     borderRadius: BorderRadius.circular(20.0),
                   ),
                  child: TextButton(
                    onPressed: (){
                      Navigator.push(context,MaterialPageRoute(builder: (context)=>Test_Login()));
                    },
                    child: Row(
                      children: [
                        Icon(
                          Icons.arrow_forward_ios,
                          color: Colors.white,
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Text('Let the adventure begin',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 24,
                          fontWeight: FontWeight.bold,
                        ),),

                      ],
                    ) ,
                  ),
                )

              ],
            ),
          ))

        ],
      ),
    );
  }
}
