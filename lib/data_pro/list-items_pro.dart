import '../model_pro/list_item_pro.dart';

final List<ListItem> listItems =[
  ListItem(
      title: 'sheraton',
      number: 0957858374,
      urlImage: 'https://images.trvl-media.com/hotels/1000000/60000/53700/53657/53b31ef5.jpg?impolicy=resizecrop&rw=670&ra=fit'
  ),
  ListItem(
      title: 'Damascus hotel',
      number: 0957858374,
      urlImage: 'https://turkey.arab-hotels.com/wp-content/uploads/2019/04/sato-otel.jpg'
  ),
  ListItem(
      title: 'four Seasons',
      number: 0957858374,
      urlImage: 'https://images.trvl-media.com/hotels/1000000/60000/53700/53657/53b31ef5.jpg?impolicy=resizecrop&rw=670&ra=fit'
  ),
  ListItem(
      title: 'Pullman Hotel',
      number: 0957858374,
      urlImage: 'https://turkey.arab-hotels.com/wp-content/uploads/2019/04/sato-otel.jpg'
  ),
];