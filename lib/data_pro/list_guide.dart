import '../model_pro/list_guide.dart';

final List<ListItem> listItems = [
  ListItem(
      title: 'Ali',
      number: '0988754646',
      rate: '5',
      urlImage:
          'https://static.vecteezy.com/system/resources/thumbnails/007/230/224/small/tour-guide-line-icon-vector.jpg'),
  ListItem(
      title: 'Ahmad',
      number: '0988754646',
      rate: '3',
      urlImage:
          'https://static.vecteezy.com/system/resources/thumbnails/007/230/224/small/tour-guide-line-icon-vector.jpg'),
  ListItem(
      title: 'Dani',
      number: '0988754646',
      rate: '2',
      urlImage:
          'https://static.vecteezy.com/system/resources/thumbnails/007/230/224/small/tour-guide-line-icon-vector.jpg'),
  ListItem(
      title: 'samir',
      number: '0988754646',
      rate: '4',
      urlImage:
          'https://static.vecteezy.com/system/resources/thumbnails/007/230/224/small/tour-guide-line-icon-vector.jpg'),
];
