import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:udemy_flutter/activity_user.dart';
import 'package:udemy_flutter/edit_places.dart';
import 'package:udemy_flutter/activity_admin.dart';
import 'editing_screen.dart';

class NaturalPage extends StatefulWidget {
  const NaturalPage({ Key? key }) : super(key: key);

  @override
  State<NaturalPage> createState() => _NaturalPageState();
}

class _NaturalPageState extends State<NaturalPage> {
void setActivity (String value){

  for (var i =0 ;i<items.length ; i++ ){
    if (value! ==items[i])
setState(() {
  activity =i;
});
  }
 
}
int activity= 0;

  // List of items in our dropdown menu
  var items = [
    'Places',
    'Hotel',
    'Resturant',
    'Activity',

  ];

  List <Widget> ActivitiesList= [
  Column(
    children: [
      TextField(
        decoration:InputDecoration(
          hintText: 'Place Name'
        ),

      ),
      TextField(
        decoration:InputDecoration(
            hintText: 'Place Location'
        ),

      ),
      TextField(
        decoration:InputDecoration(
            hintText: 'Description'
        ),

      ),
    ],
  ),
    Column(

      children: [
        TextField(
          decoration:InputDecoration(
              hintText: 'Hotel Name'
          ),

        ),
        TextField(
          decoration:InputDecoration(
              hintText: 'Hotel Number'
          ),

        ),
        TextField(
          decoration:InputDecoration(
              hintText: 'Hotel Location'
          ),

        )
,  TextField(
          keyboardType: TextInputType.number,
  decoration:InputDecoration(

  hintText: 'Hotel Rate'
  ),

  ),


  ],
    ),
    Column(
      children: [
        TextField(
          decoration:InputDecoration(
              hintText: 'Restaurant Name'
          ),
        ),
        TextField(
          decoration:InputDecoration(
              hintText: 'Restaurant Number'
          ),
        ),
        TextField(
          decoration:InputDecoration(
              hintText: 'Restaurant Location'
          ),
        )
        ,  TextField(
          keyboardType: TextInputType.number,
          decoration:InputDecoration(

              hintText: 'Restaurant Rate'
          ),

        ),
      ],
    ),
    Column(
      children: [
        TextField(
          decoration:InputDecoration(
              hintText: 'Activity Name'
          ),

        ),
        TextField(
          decoration:InputDecoration(
              hintText: 'Activity Location'
          ),

        ),
        TextField(
          decoration:InputDecoration(
              hintText: 'Description'
          ),

        ),


      ],
    ),
  ];
String dropdownvalue = 'Places';
  @override
  Widget build(BuildContext context) {
    Size _size = MediaQuery.of(context).size;
    return Scaffold(
     
      
        appBar: AppBar(
              backgroundColor: Colors.black,
              title: Text('Edit ',
                style: TextStyle(
                  fontFamily: 'OleoScriptSwashCaps',
                    fontSize: 40,fontWeight: FontWeight.bold,color: Colors.white),),
              elevation: 0,
              leading: IconButton(icon:Icon(CupertinoIcons.back,size: 35,color: Colors.white,),onPressed: (){
                Navigator.of(context).pop();
              },),
             ),
    body: Container(
              child: Column(
                children: [
                  SizedBox(
                    height: 40,
                  ),
                 Expanded(
                    child: GridView.count(
                  crossAxisCount: 2,
                  mainAxisSpacing: 20,
                  children: [
                    PopularWidget(
                        size: _size,
                        imageSrc: 'https://i0.wp.com/dirt.asla.org/wp-content/uploads/2022/01/natural-garden_dirt.jpg?ssl=1',
                        text: 'places',
                        click:(){ Navigator.of(context).push(MaterialPageRoute(builder: (context)=>EditPlaces(index:1)));}),
                    PopularWidget(
                        size: _size,
                        imageSrc: 'https://i.pinimg.com/236x/26/5d/0b/265d0b93334e5cf66e4c986a8cd0511e.jpg',
                        text: 'Hotel',
                        click:(){ Navigator.of(context).push(MaterialPageRoute(builder: (context)=>EditingScreen(type:5)));}),

                    PopularWidget(
                        size: _size,
                        imageSrc: 'https://arlingtonva.s3.amazonaws.com/wp-content/uploads/sites/25/2013/12/restaurant.jpeg',
                        text: 'Restaurant',
                        click:(){ Navigator.of(context).push(MaterialPageRoute(builder: (context)=>EditingScreen(type:4)));}),

                    PopularWidget(
                        size: _size,
                        imageSrc: 'https://cf.ltkcdn.net/camping/images/orig/257248-1600x1030-group-camping-games-activities-adults.jpg',
                      text: 'Activity',
                      click:(){ Navigator.of(context).push(MaterialPageRoute(builder: (context)=>ActivityAdmin()));}),

                  ],
                ))
          ],
        ),

    )
        );
  }
  PopularWidget({ required Size size, required String imageSrc,required String text,required click})
  {
    return Stack(
      children: [
        FlatButton (onPressed: click,
          child: Container(
            margin: EdgeInsets.symmetric(horizontal:10),
            width:size.width*0.45,height:140,
            decoration: BoxDecoration(color: Colors.grey,borderRadius: BorderRadius.circular(20)
              , image:DecorationImage(image: NetworkImage(imageSrc,),fit: BoxFit.cover,
              ),
            ),
          ),
        ),

        Positioned(
            bottom:-3,
            child:Column(children: [
              SizedBox(height:20 ),
              Row(
                children: [
                  SizedBox(width: 30,),
                  Text(text,
                      style:TextStyle(fontSize: 25,
                          fontWeight: FontWeight.bold,
                          color: Colors.black)),
                ],
              ),

            ],
            )
        ),

      ],
    );


  }
}