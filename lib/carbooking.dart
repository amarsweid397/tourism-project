

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:udemy_flutter/services/http.dart';

class CarBooking extends StatefulWidget {
  const CarBooking({ Key? key }) : super(key: key);

  @override
  State<CarBooking> createState() => _CarBookingState();
}


class _CarBookingState extends State<CarBooking> {
List <dynamic>? cars;
bool isLoading = true ; 
var length = 0; 
HttpService service = new HttpService();
  initState(){
 super.initState();
 getCars();
}
getCars ()async{
  cars =await service.getCarsData();
  if(cars!=null){
    setState(() {
      length= cars!.length;
      isLoading=false;
    });
  }
}
  @override
  Widget build(BuildContext context) {
     Size _size = MediaQuery.of(context).size;
      return Container(
      decoration: BoxDecoration(image: DecorationImage(image: NetworkImage('https://picsum.photos/500/500?random=4')
      ,fit: BoxFit.cover,
      colorFilter: ColorFilter.mode(Colors.black.withOpacity(0.5), BlendMode.darken))),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          title: Text('Cars Booking ',style: TextStyle(fontSize: 30,fontWeight: FontWeight.bold,color: Colors.white),),
            backgroundColor: Colors.transparent,
            elevation: 0,
            leading: IconButton(icon:Icon(CupertinoIcons.back,size: 35,color: Colors.white,),onPressed: (){
              Navigator.of(context).pop();
            },),
            actions: [
              IconButton(
               onPressed: (){
             showDialog(context: context, builder: (BuildContext context){
                  return AlertDialog(
                    title: Text('This interface displays the types of cars that can be booked, as each car is displayed with its picture and price, and then the reservation',
                    style: TextStyle(fontSize:25,fontWeight: FontWeight.bold,color:Colors.teal  ),),
                    
                    );

                });


          },
               icon: Icon(CupertinoIcons.question_circle,color: Colors.white,size: 30,))
            
          ],),
           

          body:   Container(
                padding:EdgeInsets.only(top: _size.height*0.05),
                child: Visibility(visible: !isLoading,
                child: GridView.builder(
                  itemCount: 6,
                  itemBuilder: (contetx,index)=>  PopularWidget(
                    id:cars![index]['id'],
                    size: _size, imageSrc: 'assets/images/car${index+1}.jpg', name:cars![index]['name'],name1: 'Price:',num: ' \$${cars![index]["price"]}'), gridDelegate:  SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                crossAxisSpacing: 5.0,
                mainAxisSpacing: 5.0,
              ),        
            

             ),
                replacement: Center(child: CircularProgressIndicator(),),)
             )




         
            )

      
    );
  }
   PopularWidget({ required Size size, required String imageSrc,required String name,required String name1,required String num, required int id})
   {
    return Stack(
      children: [
        Container(
         margin: EdgeInsets.symmetric(horizontal:10),
          width:size.width*0.45,height:140,
          decoration: BoxDecoration(color: Colors.red,borderRadius: BorderRadius.circular(20)
         , image:DecorationImage(image: AssetImage(imageSrc,),fit: BoxFit.cover,
          ),
            ),
          ),

          Positioned(
            bottom:10 ,
            child:Column(children: [
            Text(name,style:TextStyle(fontSize: 25,fontWeight: FontWeight.bold,color: Colors.white)),
              Row(children: [Text(name1,style:TextStyle(fontSize: 21,fontWeight: FontWeight.bold,color: Colors.white)),
              Text(num,style:TextStyle(fontSize: 20,fontWeight: FontWeight.bold,color: Colors.red[400]))],) ,
             SizedBox(height:5 ,),
             Row(
               children: [
                 SizedBox(width: 30,),
                 RaisedButton(onPressed: ()async{
                  await service.carTrip(id);
                 },
                 child: Text('Booking Now',style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),

                 )
                
               ],
             ),
           
          ],)),

           

      ],
    );
  
}
}