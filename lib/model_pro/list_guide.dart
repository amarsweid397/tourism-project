class ListItem {
   String title;
  final String urlImage;
  String number;
   String rate;

  ListItem(
      {required this.title,
      required this.urlImage,
      required this.number,
      required this.rate});
}
