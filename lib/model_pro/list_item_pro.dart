class ListItem {
  final String title;
  final String urlImage;
  final int number;

  ListItem({
    required this.title,
    required this.urlImage,
    required this.number
  });

}