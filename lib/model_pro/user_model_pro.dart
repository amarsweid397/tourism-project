class UserModel {
  final String name, urlImage, location, bio;
  final int countFollowers, countFollowing, countPosts;
  final List<String> urlPhotos;
  final bool isFollowing;

  UserModel({
    required this.name,
    required this.urlImage,
    required this.location,
    required this.bio,
    required this.countFollowers,
    required this.countFollowing,
    required this.countPosts,
    required this.urlPhotos,
    required this.isFollowing});
}
final users = <UserModel>[
  UserModel(
      name:'Swiming',
      urlImage:'https://www.dubaidrivingcenter.net/images/offers/DDC-offers.jpg',
      location:'united state',
      bio:'you can swim and jump',
      countFollowers:1400,
      countFollowing:100,
      countPosts:25,
      urlPhotos:[
        'https://cf.ltkcdn.net/camping/images/orig/257248-1600x1030-group-camping-games-activities-adults.jpg',
      'https://cf.ltkcdn.net/camping/images/orig/257248-1600x1030-group-camping-games-activities-adults.jpg'],
      isFollowing:true),
  UserModel(
      name:'Happy',
      urlImage:'https://cf.ltkcdn.net/camping/images/orig/257248-1600x1030-group-camping-games-activities-adults.jpg',
      location:'united state',
      bio:'you can swim and jump',
      countFollowers:1400,
      countFollowing:100,
      countPosts:25,
      urlPhotos:['https://cf.ltkcdn.net/camping/images/orig/257248-1600x1030-group-camping-games-activities-adults.jpg',
      'https://cf.ltkcdn.net/camping/images/orig/257248-1600x1030-group-camping-games-activities-adults.jpg'],
      isFollowing:true),
];

