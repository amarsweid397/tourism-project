import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:udemy_flutter/services/http.dart';

class AccountPage extends StatefulWidget {
  const AccountPage({Key? key}) : super(key: key);

  @override
  State<AccountPage> createState() => _AccountPageState();
}

class _AccountPageState extends State<AccountPage> {
  HttpService service = new HttpService();
      initState(){
      super.initState();
    }
    getTripsData()async{
      await service.getUserTrips();
    }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
        shadowColor: Colors.white,

      title: Text('My Account',style :TextStyle(fontSize:23,fontWeight: FontWeight.bold,color:Colors.black,),
      textAlign:TextAlign.center,),
      centerTitle: false,
      leading: IconButton(icon: Icon(CupertinoIcons.arrow_left,color: Colors.black,),onPressed: (){
      Navigator.of(context).pop();
    },),
    
    ),

    

    body:Padding(padding: const EdgeInsets.all(8.3),
    child: Column(
      children: [
 
    SizedBox(height: 10,),
     Positioned(
            top: 10,right: 10,
            child:Column(children: [
           
            Row(children:[
   Icon(Icons.store,color: Colors.blue,),SizedBox(width: 5,)
  ,Text('Booking Record :',style:TextStyle(fontSize: 25,fontWeight: FontWeight.bold,color: Colors.black))
            ])
          ],)),
          SizedBox(height: 20,),
          Column(
            children: [
              Container(
                height: 80,
                child: Card(
                  elevation: 3,
                  child: ListTile(
                    leading: Icon(Icons.travel_explore,color: Colors.red,),
                    title: Text('Eid Trip',style:TextStyle(fontSize: 20,fontWeight: FontWeight.bold,color: Colors.black)),
                    subtitle: Row(children: [Text('time: 9:00Am',style:TextStyle(fontSize: 15,fontWeight: FontWeight.bold,color: Colors.blue)),
                    SizedBox(width: 10,),
                    Text('date: 6/7/2022-7/7/2022',style:TextStyle(fontSize: 15,fontWeight: FontWeight.bold,color: Colors.blue))],),
                    trailing: Icon(Icons.done,color: Colors.green,),
                  ),
                ),
              ),
              Container(
                height: 80,
                child: Card(
                  elevation: 3,
                  child: ListTile(
                    leading: Icon(Icons.travel_explore,color: Colors.red,),
                    title: Text('Swim Trip',style:TextStyle(fontSize: 20,fontWeight: FontWeight.bold,color: Colors.black)),
                    subtitle: Row(children: [Text('time: 9:00Am',style:TextStyle(fontSize: 15,fontWeight: FontWeight.bold,color: Colors.blue)),
                    SizedBox(width: 10,),
                    Text('date: 6/7/2022-7/7/2022',style:TextStyle(fontSize: 15,fontWeight: FontWeight.bold,color: Colors.blue))],),
                    trailing: Icon(Icons.done,color: Colors.green,),
                  ),
                ),
              ),
              Container(
                height: 80,
                child: Card(
                  elevation: 3,
                  child: ListTile(
                    leading: Icon(Icons.travel_explore,color: Colors.red,),
                    title: Text('WeekEnd Trip',style:TextStyle(fontSize: 20,fontWeight: FontWeight.bold,color: Colors.black)),
                    subtitle: Row(children: [Text('time: 9:00Am',style:TextStyle(fontSize: 15,fontWeight: FontWeight.bold,color: Colors.blue)),
                    SizedBox(width: 10,),
                    Text('date: 6/7/2022-7/7/2022',style:TextStyle(fontSize: 15,fontWeight: FontWeight.bold,color: Colors.blue))],),
                    trailing: Icon(Icons.done,color: Colors.green,),
                  ),
                ),
              ),
              Container(
                height: 80,
                child: Card(
                  elevation: 3,
                  child: ListTile(
                    leading: Icon(Icons.travel_explore,color: Colors.red,),
                    title: Text('Cake Trip',style:TextStyle(fontSize: 20,fontWeight: FontWeight.bold,color: Colors.black)),
                    subtitle: Row(children: [Text('time: 9:00Am',style:TextStyle(fontSize: 15,fontWeight: FontWeight.bold,color: Colors.blue)),
                    SizedBox(width: 10,),
                    Text('date: 6/7/2022-7/7/2022',style:TextStyle(fontSize: 15,fontWeight: FontWeight.bold,color: Colors.blue))],),
                    trailing: Icon(Icons.done,color: Colors.green,),
                  ),
                ),
              ),
              Container(
                height: 80,
                child: Card(
                  elevation: 3,
                  child: ListTile(
                    leading: Icon(Icons.travel_explore,color: Colors.red,),
                    title: Text('Ramdan Trip',style:TextStyle(fontSize: 20,fontWeight: FontWeight.bold,color: Colors.black)),
                    subtitle: Row(children: [Text('time: 9:00Am',style:TextStyle(fontSize: 15,fontWeight: FontWeight.bold,color: Colors.blue)),
                    SizedBox(width: 10,),
                    Text('date: 6/7/2022-7/7/2022',style:TextStyle(fontSize: 15,fontWeight: FontWeight.bold,color: Colors.blue))],),
                    trailing: Icon(Icons.done,color: Colors.green,),
                  ),
                ),
              )
            ],
            
          )






      ],
    ),
    
    
    
    
    
    ),



    );
    
  }
}