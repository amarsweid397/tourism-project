
import 'package:flutter/material.dart';
import '../model_pro/list_item_pro.dart';

class ListItemWidget extends StatelessWidget{
  final dynamic item;
  final Animation<double> animation;
  final VoidCallback? onClicked;
  ListItemWidget({
    required this.item,
    required this.animation,
    required this.onClicked,
    Key?key,
}):super(key: key);

  @override
  Widget build(BuildContext context) =>
       SizeTransition( //بتحذف من تحت لفوق
           sizeFactor: animation,
        key:ValueKey(   'http://picsum.photos/500/500?random=5'),//مشان وقت عم تنحذف الصولاةعم تضرب ع صورة التحتا عم يصير في وقتكم ثانية عم تصير صورة يلي حذفتا بصورةالتحتا والتحتا عم تطول كم ثانية لترجه صورتا الاساسية وليست صورةالعنصر يلي نحذف فوقا لنحل هي المشكلة ضفنا هادال key
             child: buildItem(),
      //بتحذف من اليمين لليسار
      // SlideTransition(
      // position: Tween<Offset>(
      //   begin: Offset(-1,0),
      //   end: Offset.zero
      // ).animate(CurvedAnimation(parent: animation, curve: Curves.elasticIn),),
      //     child: buildItem(),

  );
  Widget buildItem()=>Container(
    margin: EdgeInsets.all(8),
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(12),
      color: Colors.black.withOpacity(.7)
    ),
    child: ListTile(
      contentPadding: EdgeInsets.all(16),
      leading: CircleAvatar(
        radius: 32,
        child: Icon(Icons.restaurant),
      ),
        title: Text(item['name'],
           style: TextStyle(fontSize: 20,color: Colors.white)
        ),
        subtitle:Column(children: [
          Text(item['phoneNumber']??"Not Added",
           style: TextStyle(fontSize: 20,color: Colors.white)
        ), 
        Row(children: [
for(var i =0 ; i<item['evaluation'];i++)
Icon(Icons.star , color: Colors.yellow,)

        ],)
          
        
        ],),
      trailing: IconButton(
        icon: Icon(Icons.delete,color: Colors.red,size: 32,),
        onPressed: onClicked,

      ),
    ),

  );


}