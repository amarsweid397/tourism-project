import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ItemWidget extends StatelessWidget {
  final String title;
  final int count;

  ItemWidget({required this.title,required this.count});
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(count.toString(),
          style: TextStyle(color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 15),),
        SizedBox(
          height: 4,
        ),
        Text(title,style: TextStyle(
            color: Colors.white
        ),),
      ],
    );
  }
}
