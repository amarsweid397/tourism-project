import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import '../model_pro/list_guide.dart';

class ListItemWidget extends StatefulWidget {
  final ListItem item;
  final Animation<double> animation;
  final VoidCallback? onClicked;
  ListItemWidget({
    required this.item,
    required this.animation,
    required this.onClicked,
    Key? key,
  }) : super(key: key);
  bool editing = false;
  @override
  State<ListItemWidget> createState() => _ListItemWidgetState();
}

class _ListItemWidgetState extends State<ListItemWidget> {
  @override
  Widget build(BuildContext context) {
    int rate = int.parse(widget.item.rate);

    return SizeTransition(
        //بتحذف من تحت لفوق
        sizeFactor: widget.animation,
        key: ValueKey(widget.item
            .urlImage), //مشان وقت عم تنحذف الصولاةعم تضرب ع صورة التحتا عم يصير في وقتكم ثانية عم تصير صورة يلي حذفتا بصورةالتحتا والتحتا عم تطول كم ثانية لترجه صورتا الاساسية وليست صورةالعنصر يلي نحذف فوقا لنحل هي المشكلة ضفنا هادال key
        child: buildItem()
        //بتحذف من اليمين لليسار
        // SlideTransition(
        // position: Tween<Offset>(
        //   begin: Offset(-1,0),
        //   end: Offset.zero
        // ).animate(CurvedAnimation(parent: animation, curve: Curves.elasticIn),),
        //     child: buildItem(),
        );
  }

  Widget buildItem() {
    var txt = TextEditingController();
    txt.text = widget.item.number;
    return Container(
      margin: EdgeInsets.all(8),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        color: Colors.white,
      ),
      child: ListTile(
        contentPadding: EdgeInsets.all(16),
        leading: CircleAvatar(
          radius: 32,
          backgroundColor: Colors.grey,
          backgroundImage: NetworkImage(widget.item.urlImage),
        ),
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(widget.item.title,
                style: TextStyle(fontSize: 20, color: Colors.black)),
            SizedBox(
              height: 2,
            ),
            Row(
              children: [
                !widget.editing
                    ? Text(widget.item.number,
                        style: TextStyle(fontSize: 20, color: Colors.blue))
                    : SizedBox(
                        height: 60,
                        width: 100,
                        child: TextField(
                          controller: txt,
                          keyboardType: TextInputType.number,
                        ),
                      ),
                IconButton(
                  icon: IconButton(
                      onPressed: () {
                        if (widget.editing) {
                          setState(() {
                            widget.item.number = txt.text;
                          });
                        }
                        setState(() {
                          widget.editing = !widget.editing;
                        });
                        print(widget.editing);
                      },
                      icon: widget.editing == false
                          ? Icon(
                              Icons.edit,
                              color: Colors.grey,
                              size: 20,
                            )
                          : Icon(
                              Icons.done,
                              color: Colors.green,
                              size: 20,
                            )),
                  onPressed: () {},
                ),
              ],
            ),
            SizedBox(
              height: 1,
            ),
            Row(children: [
              for (var i = 0; i < int.parse(widget.item.rate); i++)
                Icon(
                  Icons.star,
                  color: Colors.yellow,
                  size: 25,
                )
            ]),
          ],
        ),
        trailing: IconButton(
          icon: Icon(
            Icons.delete,
            color: Colors.red,
            size: 32,
          ),
          onPressed: widget.onClicked,
        ),
      ),
    );
  }
}
