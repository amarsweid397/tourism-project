import 'package:flutter/material.dart';
import 'package:udemy_flutter/model_pro/user_model_pro.dart';
class BuildProfile extends StatelessWidget {
final UserModel user;
BuildProfile({required this.user});
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.all(24.0),
        child: Column(
          children: [
            Expanded(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                 Expanded(child:_buildUser(user), ) ,
                  _followButtom(user)

                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  _buildUser(UserModel user) {
    return  Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(user.name,
        style: TextStyle(
          fontSize: 16,
          fontWeight: FontWeight.bold,
        ),),
        SizedBox(height: 4,),
        Text(user.location),
      ],
    );
  }

  _followButtom(UserModel user) {
    return GestureDetector(
     // onTap: widget.onPressed(),
      child: AnimatedContainer(duration: Duration(milliseconds: 300),
        curve: Curves.easeIn,
        width: user.isFollowing?50:200,
        height: 50,
        child: user.isFollowing?buildUnFollow():buildFollow(),
      ),
    );
  }

  buildUnFollow() {
    return Container(
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(25),
      color: Colors.teal
    ),
    child: Icon(Icons.people,color: Colors.white,),
  );}
buildFollow(){
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(25),
          color: Colors.white
      ),
      child: FittedBox( child: Text(
        'follow',
        style: TextStyle(
          color: Colors.tealAccent,
          letterSpacing: 1.5,fontWeight: FontWeight.bold
        ),
      ))
    );
}
}
