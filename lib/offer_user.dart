import 'package:flutter/material.dart';
import 'package:udemy_flutter/objects/offert.dart';
import 'package:udemy_flutter/services/http.dart';

import 'objects/offert_taken.dart';
class OffersUser extends StatefulWidget {
  const OffersUser({Key? key}) : super(key: key);
  @override
  State<OffersUser> createState() => _OffersUserState();
}
class _OffersUserState extends State<OffersUser> {
  List<Datum>? offers;
  bool isLoading  = true  ; 
  var length = 0 ;
  HttpService service = new HttpService ();
getData()async{
  offers= await service.getOffertsUser();
  if(offers!=null){
    setState(() {
      isLoading= false ;
      length = offers!.length;

    });
  }
}
@override
  void initState() {
    // TODO: implement initState
    super.initState();
    getData();
  }
  @override
  Widget build(BuildContext context) {
    Size _size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        shadowColor: Colors.white,
        title: Text('Offers',style :TextStyle(fontSize:23,fontWeight: FontWeight.bold,color:Colors.black,),
          textAlign:TextAlign.center,),
        centerTitle: false,
        leading: IconButton(icon: Icon(Icons.arrow_left,color: Colors.black,),onPressed: (){
          Navigator.of(context).pop();
        },),
      ),
      body: ListView(
        
        shrinkWrap: true,
      scrollDirection:Axis.vertical,
      children: [
        Stack(children: [Container(
        width:_size.width ,
          height: _size.height/2.4,
          decoration: BoxDecoration( borderRadius: BorderRadius.only(bottomLeft: Radius.circular(50),
              bottomRight: Radius.circular(50))
              , image:DecorationImage(
                  image: NetworkImage('https://edubai.life/wp-content/uploads/2019/01/best-offer.jpg')
                  ,fit:BoxFit.cover )),
        ),
        ],),
        Padding(padding: const EdgeInsets.all(8.3),
          child: Column(
            children: [
              SizedBox(height: 10,),
              Positioned(
                  top: 10,right: 10,
                  child:Column(children: [

                    Row(children:[
                      Icon(Icons.local_grocery_store_rounded,color: Colors.blue,),SizedBox(width: 5,)
                      ,Text('Offers :',style:TextStyle(fontSize: 25,fontWeight: FontWeight.bold,color: Colors.black))
                    ])
                  ],)),
              SizedBox(height: 20,),
           Visibility(
            visible: !isLoading,
            replacement: CircularProgressIndicator.adaptive(),
            
            
            child: ListView.builder(itemCount: length,
              shrinkWrap: true,
              itemBuilder: (context,index){
            return       Container(
                    height: 80,
                    child: Card(
                      elevation: 3,
                      child: ListTile(
                        leading: Icon(Icons.percent_rounded,color: Colors.lightBlueAccent,),
                        title: Text(offers![index].tripName,style:TextStyle(fontSize: 20,fontWeight: FontWeight.bold,color: Colors.black)),
                        trailing: Icon(Icons.label_important_outlined,color: Colors.blue,),
                        onTap: (){
                          showDialog(
                          barrierDismissible: false,
                          context: context,
                          builder: (context) {
                          return AlertDialog(
                          title: Text('Details Offers:',
                          style: TextStyle(
                          color: Colors.blueAccent,
                          fontFamily: 'Courgette'
                          ),
                          ),
                          content: Container(
                          height: 128,
                          child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                          Text( offers![index].tripName),
                          SizedBox(
                          height: 10,
                          ),
                          Text('Start As: ${offers![index].startsAt}'),
                          SizedBox(
                          height: 10,
                          ),
                          Text('Ends At :${offers![index].endsAt}'),
                          SizedBox(
                          height: 10,
                          ),
                          Text('percentage:${offers![index].costAfterOffer*100/offers![index].cost}%' ),
                          ]),
                          ),
                          actions: [
                          TextButton(
                          onPressed: () {
                          Navigator.of(context).pop();
                          },
                          child: Text('Cancel '))
                          ],
                          );
                          });
                          },
                      ),
                    ),
                  );
           }))
            ],
          ),
        ),
    ]
    ),
    );
  }
}
