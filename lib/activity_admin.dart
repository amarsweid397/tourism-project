import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:udemy_flutter/model_pro/product.dart';
import 'package:udemy_flutter/services/http.dart';
import 'data_pro/list_activity_data.dart';
import 'model_pro/list_activity.dart';
import 'model_pro/product.dart';
class ActivityAdmin extends StatefulWidget {
  const ActivityAdmin({Key? key}) : super(key: key);

  @override
  State<ActivityAdmin> createState() => _ActivityAdminState();
}

class _ActivityAdminState extends State<ActivityAdmin> {
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: (){},
        backgroundColor: Colors.black,child: Icon(Icons.add),),
      backgroundColor: Colors.grey[100],
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text('Activity ',
          style: TextStyle(
              fontFamily: 'OleoScriptSwashCaps',
              fontSize: 40,fontWeight: FontWeight.bold,color: Colors.white),),
        elevation: 0,
        leading: IconButton(icon:Icon(CupertinoIcons.back,size: 35,color: Colors.white,),onPressed: (){
          Navigator.of(context).pop();
        },),
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(40.0),
          child: Column(
            children: [
              Container(
                height:485,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20)
                ),
                child: Swiper(
                  itemCount:Products.length,
                  itemWidth: 300,
                  layout: SwiperLayout.STACK,
                  itemBuilder: (context,index){
                    return Stack(
                      children: [
                        SizedBox(
                          height: 600,
                          width: 300,
                          child: Card(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20)
                            ),
                            color: Colors.white,
                            child: Column(
                              children: [
                                Container(
                                  height:300,
                                  width:double.infinity,
                                  child:
                                  ClipRRect(
                                    child:    Image(image: AssetImage(Products[index].image),fit: BoxFit.fill),
                                    borderRadius: BorderRadius.circular(20),
                                  ),)
                                ,
                                // Image(image: NetworkImage('https://picsum.photos/500/500?random=$index',)),
                                // Image.asset('assets/images/ph.jpg'
                                // ,width: 300,
                                //   height: 200,
                                //   fit: BoxFit.cover,
                                // ),
                                Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 15,
                                      vertical: 10),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text(Products[index].title,
                                        style: TextStyle(
                                          fontSize: 24,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Text(Products[index].description,
                                        style: TextStyle(
                                          fontSize: 14,
                                        ),
                                      ),
                                      SizedBox(
                                        height: 2.0,
                                      ),
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                       Row(children: [
                                           Icon(Icons.location_on,
                                            color: Colors.blue,),
                                                Text(Products[index].location,
                                            style: TextStyle(
                                                color: Colors.blue
                                            ),),
                                       ],),
                                                         Icon(Icons.delete,
                                            color: Colors.red,),
                                          SizedBox(
                                            width: 10.0,
                                          ),
                                      
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    );
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}



