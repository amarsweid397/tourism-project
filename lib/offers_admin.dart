import 'package:flutter/material.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:udemy_flutter/services/http.dart';

import 'objects/offert.dart';
import 'objects/offert_taken.dart';

class OfferAdmin extends StatefulWidget {
  const OfferAdmin({Key? key}) : super(key: key);

  @override
  State<OfferAdmin> createState() => _OfferAdminState();
}

class _OfferAdminState extends State<OfferAdmin> {
  final panelController = PanelController();
  TextEditingController statAtController = TextEditingController();
    TextEditingController percentageController = TextEditingController();
      TextEditingController endsAtController = TextEditingController();
      
  int index = 0;
  List<Offer>? offerts;
  var length  =0 ; 
  bool isLoading = true;
  HttpService service = new HttpService();
  TextEditingController nameController  = TextEditingController();
  getOffertsData ()async{
offerts = await service.getOfferts();
if(offerts!=null){
  setState(() {
    isLoading = false; 
    length = offerts!.length;
  });
}
  }
  @override
   initState() {

 
    getOffertsData();
       super.initState(); 
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        //مشان ال body يصير ورا appbar
        extendBodyBehindAppBar: true,
        floatingActionButton: FloatingActionButton(backgroundColor: Colors.black,onPressed: (){
          showDialog(context: context, builder: (_)=>AlertDialog(
            content: Container(height:250,child: Column(children: [
            TextField(
              controller: nameController,
              decoration: InputDecoration(hintText: 'Offer Name'),),
                     TextField(
                                              keyboardType:TextInputType.text,
                      controller: statAtController,
                      decoration: InputDecoration(hintText: 'Offer start date'),),
                              TextField(
                                 keyboardType:TextInputType.text,
                                controller: endsAtController,
                                
                                decoration: InputDecoration(hintText: 'Offer end date'),),
                                       TextField(
                                        keyboardType:TextInputType.text,
                                        controller:percentageController,
                                        decoration: InputDecoration(hintText: 'Offer precntage'),),
                                       TextButton(onPressed: ()async{

                                        await service.addNewOffer(nameController.text, statAtController.text.toString(),
                                         percentageController.text.toString(), percentageController.text);
                                         Navigator.pop(context);
                                         setState(() {
                                           isLoading=true ; 
                                         });
                                         await getOffertsData();
                                       }, child: Text('Add New Offer'))
            ]), ),
          ));
        },
        child: Icon(Icons.add),
        ),
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.transparent,
          leading: IconButton(
            icon: Icon(Icons.person_outlined),
            onPressed: () {},
          ),
          actions: [
            IconButton(
              icon: Icon(Icons.close),
              onPressed: () {},
            ),
          ],
        ),
        body: SlidingUpPanel(
          maxHeight: 600,
          minHeight: 400,
          parallaxEnabled: true,
          parallaxOffset: 0.5,
          controller: panelController,
          color: Colors.transparent,
          body: PageView.builder(
            itemBuilder: (context, index) {
              return Container(
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: NetworkImage(
                            'https://wallpaperaccess.com/full/1241095.jpg'),
                        fit: BoxFit.cover)),
              );
            },
          ),
          panelBuilder: (ScrollController scroll) {
            return Column(
              children: [
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 16, vertical: 12),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        " Offers :",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 50,
                            color: Colors.white,
                            fontFamily: 'Courgette'),
                      ),
                      // ItemWidget(title: 'Follower',count: users[index].countFollowers,),
                      // ItemWidget(title: 'posts',count: users[index].countPosts,),
                      // ItemWidget(title: 'Following',count: users[index].countFollowing,),
                    ],
                  ),
                ),
                Expanded(
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30),
                        topRight: Radius.circular(30),
                      ),
                      color: Colors.white,
                    ),
                    child: ListView(children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 10,
                        ),
                        child:Visibility(visible: !isLoading,
                        child:  ListView.separated(
                            physics: NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            itemBuilder: (context, index) =>bild(
                              onEdit: (){
showDialog(context: context, builder: (context){
return AlertDialog(content: Container(height: 100,child: Column(children: [
  TextField(
    
    controller: nameController,
    decoration: InputDecoration(hintText: 'Name'),),
  TextButton(onPressed: ()async{
    
    await service.updateOffer( offerts![index].id, nameController.text, offerts![index].startsAt, offerts![index].endsAt,offerts![index].percentage);
    Navigator.pop(context);
    setState(() {
    isLoading =true;
    });
    await getOffertsData();
 } ,child: Text('Done'))
  
]),),);
});
                              },
                              percentage: offerts![index]. percentage.toString(),
                                                     content: offerts![index].name,
starts_at:offerts![index].startsAt.toString() ,

ends_at:offerts![index].startsAt.toString() ,

                            onClick: ()async{
                    await        service.deleteOffer( offerts![index].id);
                    setState(() {
                      isLoading= true;
                    });
                    await getOffertsData();
                            }
                            ),
                            separatorBuilder: (context, index) => SizedBox(
                                  height: 4,
                                ),
                            itemCount:length),replacement: Center(child: CircularProgressIndicator()),)
                      ),
                    ]),
                  ),
                ),
              ],
            );
          },
        ));
  }

  Widget bild({required String content , required  VoidCallback onClick,required VoidCallback onEdit, 
  required String percentage,
   required String starts_at , required String ends_at }) {
    
    return
    Padding(
        padding: const EdgeInsets.symmetric(vertical: 6),
        child: Container(
          child: Card(
            color: Colors.white.withOpacity(.7),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
            elevation: 10,
            shadowColor: Colors.blue,
            child: ListTile(
              leading: CircleAvatar(
                backgroundColor: Colors.white38,
                radius: 18,
                backgroundImage: NetworkImage(
                    'https://icon-library.com/images/discount-icon/discount-icon-16.jpg'),
              ),
              title: Row(
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        content,
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                            color: Colors.blue),
                      ),
                      Text(
                        'begin $starts_at ',textAlign: TextAlign.end,
                        style: TextStyle(
                            fontSize: 10, fontWeight: FontWeight.bold),
                      ),
                        Text(
                        'ends $ends_at ',textAlign: TextAlign.end,
                        style: TextStyle(
                            fontSize: 10, fontWeight: FontWeight.bold),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left:10),
                       child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                             Text(
                    'Percentage: $percentage',textAlign: TextAlign.end,
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                            IconButton(
                              onPressed:
                                onClick,
                              
                              icon: 
                            Icon(
                              Icons.delete,
                              color: Colors.red,
                            ),),
                                 IconButton(
                              onPressed:
                                onEdit  ,
                              
                              icon: 
                            Icon(
                              Icons.edit,
                              color: Colors.yellow,
                            ),),
                          ],
                        )
                      ),
                    ],
                  ),
                  SizedBox(
                    width: 3,
                  ),

                ],
              ),

            ),
          ),
        ),
      );
}}
