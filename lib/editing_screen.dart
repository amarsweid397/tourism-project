import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:udemy_flutter/services/http.dart';
import 'package:udemy_flutter/uitils_pro/custom_clipper.dart';
import 'package:udemy_flutter/widget_pro/list_item_widget_pro.dart';
import '../model_pro/list_item_pro.dart';
import 'package:udemy_flutter/data_pro/list-items_pro.dart';
class EditingScreen extends StatefulWidget {
  final int  type ;

  const EditingScreen({required this.type}) ; 
 
  @override
  State<EditingScreen> createState() => _EditingScreenState();
}

class _EditingScreenState extends State<EditingScreen> {
  final listKey =GlobalKey<AnimatedListState>();
  final List<ListItem> items =List.from(listItems);
  
    bool isLoading = true ;
List<dynamic>? resturantsData ; 
  HttpService service = new HttpService(); 
  int length = 0 ; 
  
  getData()async{
   resturantsData=  await service.getPlacesData(widget.type, 1);
    if(resturantsData!=null){
      setState(() {
        isLoading = false ; 
      length = resturantsData!.length ; 
      });
    

    }
  }
  TextEditingController nameController = new TextEditingController();
  TextEditingController phoneController = new TextEditingController();
  TextEditingController rateController = new TextEditingController();
  TextEditingController typeController = new TextEditingController();
  void initState() {
    // TODO: implement initState
    super.initState();
    getData();
  }
  @override
  Widget build(BuildContext context) {

    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: NetworkImage('https://imageio.forbes.com/specials-images/imageserve/5f9cca07d4c42920d4d348c7/rainforest/960x0.jpg?format=jpg&width=960'),
          fit:BoxFit.cover,
          colorFilter: ColorFilter.mode(
            Colors.black.withOpacity(0.6),
            BlendMode.darken,
          ),
        ),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(100),
          child: AppBar(
            centerTitle: true,
            title: Text(
              ' Editing',
              style: TextStyle(
                color: Colors.green,
                fontSize: 36,
                fontWeight: FontWeight.bold,
              ),
            ),
            backgroundColor: Colors.transparent,
            elevation: 0.0,
            flexibleSpace: ClipPath(
              clipper: MyCustomClipperForAppBar(),
              child: Container(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [Colors.black.withOpacity(0.8),Color(0xFF292C36)],
                    begin: Alignment.bottomRight ,
                    end: Alignment.topLeft,
                    tileMode: TileMode.clamp,
                  )
                ),
              ),
            ),
          ),
        ),
        // appBar: AppBar(
        //   toolbarHeight: 80,
        //   //shadowColor: Color.fromARGB(10,12, 18,1),
        //   centerTitle: true,
        //   title: Text('..Editing..',
        //     style: TextStyle(
        //       color: Colors.black,
        //       fontWeight: FontWeight.bold,
        //       fontSize: 45,
        //     ),
        //   ),
        //   flexibleSpace: Container(
        //     decoration: BoxDecoration(
        //       borderRadius: BorderRadius.only(bottomLeft: Radius.circular(28),
        //           bottomRight: Radius.circular(28)),
        //       gradient: LinearGradient(
        //         colors: [Colors.red,Colors.pink],
        //         begin: Alignment.bottomCenter ,
        //         end: Alignment.topCenter,
        //       )
        //     ),
        //   ),
        //   backgroundColor: Colors.transparent,
        //   elevation: 0.0,
        //   brightness: Brightness.dark,
        //   // shape: RoundedRectangleBorder(
        //   //   borderRadius:BorderRadius.only(bottomLeft: Radius.circular(50),
        //   //       bottomRight: Radius.circular(50))
        //   // ),
        // ),

        body: Visibility
        (
          visible:!isLoading ,
          replacement: Center(child: CircularProgressIndicator()),
          child: AnimatedList(
          key:listKey,
          initialItemCount:length,
          itemBuilder: (context,index,animation)=>ListItemWidget(
            item:resturantsData![index][0],
            animation:animation,
            onClicked:()async{
              await service.deletePlace(resturantsData![index][0]['id']);
            setState(() {
              isLoading = true ; 

            });
            await getData();
            },
          ),
        ),),
        floatingActionButton: FloatingActionButton(onPressed:(){
        showDialog(context: context, builder: (context){
        return AlertDialog(title: Text('Add new resturant '),
        content:Container(
          height:300 ,
          child: Column(
          children: [
            TextField(
              controller: nameController,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(hintText: 'Resturant Name'),
            ),
                 TextField(
                  controller: phoneController,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(hintText: 'Resturant Number'),
            ),
                TextField(
                  controller: rateController,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(hintText: 'Resturant Rate'),
            ),
           
                Center(child: 
                 TextButton(onPressed: ()async{
if(nameController.text!=''&&phoneController!=''&&rateController!=''){
  await service.addNewResturantHotel(widget.type.toString(), nameController.text.toString(), phoneController.text.toString(), rateController.text.toString());
 Navigator.pop(context);
  setState(() {
    isLoading = true ; 

  });
  await getData();
}
                 }, child: Text('Add New Resturant')),)
          ],
        ),
        ),
    
        );  
        })
;        }
          ,backgroundColor: Colors.black,
        child: Icon(Icons.add),),

      ),
    );
  }
  void insertItem()
  {
    //مشان اذا لاضيف عنصر بس
    // final newItem=ListItem(title: 'Orange',
    //     urlImage: 'https://es.la-croix.com/images/2000/que-es-el-derecho-natural.jpg');
    final newIndex =1; //اذا صفر فرح يبلش اضافة من البداية اما 1 فرح يبلش اضافة من بعد اول عنصر موجود
    //هون عم بضيف مننفس القائمة عناصر عشوائية منا من خلال .first
    final newItem=(List.of(listItems)..shuffle()).first;
   items.insert(newIndex, newItem) ;
   listKey.currentState!.insertItem
     (
     newIndex,
     duration: Duration(milliseconds: 600),
   );
}
void  removedItem (int index){
    final removedItem = items[index];
    items.removeAt(index);
    listKey.currentState!.removeItem(index, (context, animation) => ListItemWidget(
        item: removedItem,
        animation: animation,
        onClicked: (){}
    ),
      duration: Duration(milliseconds: 600),

    );
}
}


