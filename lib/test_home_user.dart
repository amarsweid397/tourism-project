

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:udemy_flutter/home_admin.dart';
import 'package:udemy_flutter/login_project.dart';
class TestHomeUser extends StatefulWidget {
  const TestHomeUser({Key? key}) : super(key: key);

  @override
  State<TestHomeUser> createState() => _TestHomeUserState();
}

class _TestHomeUserState extends State<TestHomeUser> {
  final urlImages=[
    'https://i.pinimg.com/564x/96/28/fd/9628fdba9736f35a6abc0c424b934e07.jpg',
    'https://i.pinimg.com/736x/37/92/79/3792792b823d6fd604906790564e899b.jpg',
    'https://i.pinimg.com/736x/d1/f2/c2/d1f2c26c285891713631a5562087278c.jpg',
    'https://i.pinimg.com/564x/07/c6/98/07c698c3f8aa27d9c60068c7bfe3edce.jpg',
    'https://i.pinimg.com/564x/64/08/c6/6408c618c3a8ec557c0a7a57694cad30.jpg',
    'https://i.pinimg.com/564x/25/1c/4b/251c4b01548c50c8fd70fcce1b259949.jpg',
  ];
  GlobalKey<ScaffoldState> _key =new  GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    Size _size =MediaQuery.of(context).size;
    return Scaffold(
      drawer: Drawer(backgroundColor: Colors.white,
      child:TextButton(child: Text('Log out'),onPressed: ()async{
          final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
      final SharedPreferences prefs = await _prefs;
      await prefs.remove('name');
      Navigator.of(context).push(MaterialPageRoute(builder: (_)=>Test_Login()));
      },),
      ),
      key: _key,
      body: ListView(
        scrollDirection: Axis.vertical,
        children: [
          Stack(
            children: [
              Container(
                width: _size.width,
                height: _size.height/2,
                child: Swiper(
                  itemCount: 6,
                  layout: SwiperLayout.CUSTOM,
                  customLayoutOption:CustomLayoutOption(
                      startIndex: 0,
                      stateCount: 3
                  ).addScale([0.7,0.85,1],
                      Alignment.bottomCenter).addTranslate(
                      [
                        Offset(0,40),
                        Offset(0,-10),
                        Offset(0,-50),
                        Offset(0,-100),
                      ]).addOpacity([0.5,.85,1,1]),
                  itemWidth: double.infinity,
                  loop: true,
                  //autoplay: true,
                  itemBuilder: (BuildContext context,int index)=>ClipRRect(
                    borderRadius: BorderRadius.only(bottomLeft: Radius.circular(50),
                        bottomRight: Radius.circular(50)),
                    child: Container(
                      decoration: BoxDecoration(image: DecorationImage(
                          image: NetworkImage('http://picsum.photos/500/500?random=$index')
                      )),
                    ),
                  ),
                ),
              ),
              Positioned(
                top: 45,
                left: 20,
                child: Text('Activity :)',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 35,
                      color: Colors.white70
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 0,left: 5),
                child: Row(
                  children: [
                    IconButton(onPressed: (){
                      _key.currentState!.openDrawer();
                    },
                        icon: Icon(
                          Icons.menu,
                          size: 30,
                          color: Colors.white70,))
                  ],
                ),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(left: 15),
            child: Row(
              children: [
                Text('My Action :',
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 35,
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal:15),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  tripBtn(content: 'Single tripe',
                      icon:Icons.ac_unit,
                      color: Colors.green,
                      myFunc:(){
                        Navigator.of(context).
                        push(MaterialPageRoute(builder: (context)=> TestHomeUser()));
                      }),
                  tripBtn(content: 'Group tripe',
                      icon:Icons.ac_unit,
                      color: Colors.green,
                      myFunc:(){
                        Navigator.of(context).
                        push(MaterialPageRoute(builder: (context)=> HomeAdmin()));
                      }),
                ]
            ),
          ),
          //موديلات ازرار
          // Row(
          //   children: [
          //    ElevatedButton(onPressed: (){},
          //      child: Text('zxcvbgbbmmm'),
          //    style: ButtonStyle(shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          //      RoundedRectangleBorder(
          //        borderRadius: BorderRadius.circular(20),
          //        side: BorderSide(color: Colors.black),
          //      ),
          //    )),),
          //     TextButton(onPressed: (){}, child: Text('dccccccdd'),
          //       style: ButtonStyle(
          //        foregroundColor: MaterialStateProperty.all<Color>(Colors.black),
          //       shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          //       RoundedRectangleBorder(
          //           borderRadius: BorderRadius.circular(20),
          //         side: BorderSide(color: Colors.black)
          //       ),
          //     ),
          //     ),
          //     )
          //   ],
          // ),
          Padding(padding: EdgeInsets.only(left: 15,top: 15),
            child: Text('Popular :',
              textAlign: TextAlign.left,
              style: TextStyle(
                  fontSize: 30,
                  fontWeight: FontWeight.bold
              ),),
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 15),
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  PopularWidget(
                      size: _size,
                      imageSrc: 'https://i.pinimg.com/564x/96/28/fd/9628fdba9736f35a6abc0c424b934e07.jpg',
                      rating: '5.7', name: "Miroo"),
                  PopularWidget(
                      size: _size,
                      imageSrc: 'http://picsum.photos/500/500?random=2',
                      rating: '6.8', name: "ram"),
                  PopularWidget(
                      size: _size,
                      imageSrc: 'http://picsum.photos/500/500?random=3',
                      rating: '8.1', name: "maaaaa"),
                  PopularWidget(
                      size: _size,
                      imageSrc: 'http://picsum.photos/500/500?random=4',
                      rating: '3.7', name: "meee")
                ],
              ),
            ),
          ),
        ],
      ),
    );
    // return Scaffold(
    //   body: GridView.count(crossAxisCount: 2,
    //   children: List.generate(6, (index) => Container(
    //   child: Padding(
    //     padding: EdgeInsets.all(20),
    //     child: Container(
    //       decoration: BoxDecoration(
    //         borderRadius: BorderRadius.circular(20),
    //       ),
    //       clipBehavior: Clip.antiAliasWithSaveLayer,
    //       child: Stack(
    //         alignment: Alignment.bottomCenter,
    //         children:
    //         [
    //           Image(
    //             image: NetworkImage('https://i.pinimg.com/236x/ef/51/53/ef51539297ddac48b825fbd614273bf8.jpg'),
    //             width: 269,
    //             height: 269,
    //             fit: BoxFit.cover,
    //           ),
    //           Container(
    //             width: 269 ,
    //             color: Colors.black.withOpacity(.4),
    //             padding: EdgeInsets.symmetric(
    //               vertical: 10.0,
    //               //horizontal: 10.0,
    //             ),
    //             child: Text(
    //               'Flower',
    //               textAlign: TextAlign.center,
    //               style: TextStyle(
    //                 fontSize: 20,
    //                 color: Colors.white,
    //               ),
    //             ),
    //           ),
    //         ],
    //       ),
    //     ),
    //   ),
    //   ),
    //   ),
    //   ),
    //   // body: SingleChildScrollView(
    //   //   child: Column(
    //   //     crossAxisAlignment: CrossAxisAlignment.start,
    //   //     children: [
    //   //       Container(
    //   //         child: Swiper(
    //   //           itemBuilder: (BuildContext context, int index)
    //   //           {
    //   //           final urlImage = urlImages[index];
    //   //           return ClipRRect(
    //   //             borderRadius: BorderRadius.circular(40),
    //   //             child:Image.network(
    //   //               urlImage,
    //   //               fit: BoxFit.cover,),
    //   //           );
    //   //               },
    //   //                 itemCount:urlImages.length,
    //   //                 itemHeight:340,
    //   //                 itemWidth:double.infinity,
    //   //                 viewportFraction: 0.9,
    //   //                 scale: 0.9,
    //   //                 layout: SwiperLayout.STACK,
    //   //                 // autoplay: true,
    //   //
    //   //         ),
    //   //       ),
    //   //       SizedBox(
    //   //         height: 20,),
    //   //       Text(
    //   //           'My Offers :',
    //   //         style: TextStyle(
    //   //           fontWeight: FontWeight.bold,
    //   //           fontSize: 46
    //   //         ),
    //   //       ),
    //   //       SizedBox(
    //   //         height: 20,),
    //         // Card(
    //         //   color: Colors.blue,
    //         //   child: Container(
    //         //     decoration: BoxDecoration(
    //         //       image: DecorationImage(
    //         //         image: NetworkImage('https://i.pinimg.com/236x/ef/51/53/ef51539297ddac48b825fbd614273bf8.jpg',),
    //         //         fit: BoxFit.cover,
    //         //         alignment: Alignment.topCenter
    //         //       )
    //         //     ),
    //         //   ),
    //         // ),
    //         // Padding(
    //         //   padding: const EdgeInsets.all(10.0),
    //         //   child: Row(
    //         //     children: [
    //         //       Container(
    //         //         height: 200,
    //         //        decoration: BoxDecoration(
    //         //          borderRadius: BorderRadius.circular(30)
    //         //        ),
    //         //        child:Card(
    //         //          elevation: 5.0,
    //         //          child: Image.network('https://i.pinimg.com/236x/ef/51/53/ef51539297ddac48b825fbd614273bf8.jpg',
    //         //          fit: BoxFit.cover,
    //         //          ),
    //         //        ),
    //         //       ),
    //         //       SizedBox(
    //         //         width: 40,),
    //         //       Container(
    //         //         height: 200,
    //         //         decoration: BoxDecoration(
    //         //             borderRadius: BorderRadius.circular(30)
    //         //         ),
    //         //         child:Card(
    //         //           elevation: 5.0,
    //         //           child: Image.network('https://i.pinimg.com/236x/89/f6/f9/89f6f9534006a5157b9190b2298a12be.jpg',
    //         //             fit: BoxFit.cover,),
    //         //         ),
    //         //       ),
    //         //     ],
    //         //   ),
    //         // ),
    //         // SizedBox(
    //         //   height: 20,),
    //         // Padding(
    //         //   padding: const EdgeInsets.all(10.0),
    //         //   child: Row(
    //         //     children: [
    //         //       Container(
    //         //         width: 170,
    //         //         height: 200,
    //         //         decoration: BoxDecoration(
    //         //             borderRadius: BorderRadius.circular(30)
    //         //         ),
    //         //         child:Card(
    //         //           elevation: 5.0,
    //         //           child: Image.network('https://i.pinimg.com/236x/d6/e2/31/d6e231ad2e5653a5a3b350662642e9b4.jpg',
    //         //             fit: BoxFit.cover,),
    //         //         ),
    //         //       ),
    //         //       SizedBox(
    //         //         width: 40,),
    //         //       Container(
    //         //         width: 170,
    //         //         height: 200,
    //         //         decoration: BoxDecoration(
    //         //             borderRadius: BorderRadius.circular(30)
    //         //         ),
    //         //         child:Card(
    //         //           elevation: 5.0,
    //         //           child: Image.network('https://campspace.com/media/medium/uploads/space/22/83/_3/2283_3_5cf8d1d9a5393.JPG',
    //         //             fit: BoxFit.cover,),
    //         //         ),
    //         //       ),
    //         //     ],
    //         //   ),
    //         // ),
    //     //  ],
    //   //  ),
    //  // ),
    // );
  }
  RaisedButton tripBtn(
      {required String content,required IconData icon,required Color color,required myFunc})
  {
    return RaisedButton(color: color,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 10,vertical: 5),
        child: Row(
          children: [
            Icon(
              icon,
              size: 18,
              color: Colors.indigo,
            ),
            Text(
              content,
              style: TextStyle(
                  color:Colors.white ,
                  fontSize: 18),
            ),
          ],
        ),
      ),
      onPressed: myFunc,
    );
  }

  PopularWidget(
      {required Size size,required String imageSrc,required String rating,required String name})
  {
    return Stack(
      children: [
        Container(
          margin:EdgeInsets.symmetric(horizontal: 20),
          width: size.width*0.45,
          height: 200,
          decoration: BoxDecoration(
              color: Colors.yellow,
              borderRadius: BorderRadius.circular(20),
              image: DecorationImage(
                image: NetworkImage(imageSrc),
                fit: BoxFit.cover,
              )
          ),
        ),
        Positioned(
            top: 20,
            left: 30,
            child: Column(
              children: [
                Text(
                  name,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 22,
                      color: Colors.black87
                  ),
                ),
                Row(
                  children: [
                    Icon(
                      CupertinoIcons.star_fill,
                      color: Colors.yellow,),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      rating,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 23,
                          color: Colors.black
                      ),
                    ),
                  ],
                )
              ],
            ))
      ],
    );
  }
}