import 'package:flutter/material.dart';
class ResetPassword extends StatefulWidget {
  const ResetPassword({Key? key}) : super(key: key);

  @override
  State<ResetPassword> createState() => _ResetPasswordState();
}

class _ResetPasswordState extends State<ResetPassword> {
  String password ='';

  bool isPasswordVisible = false;

  var emailController = TextEditingController();

  var passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //backgroundColor: Colors.transparent,
     body:Container(
       decoration: BoxDecoration(
         image: DecorationImage(
           image: NetworkImage('https://img.freepik.com/free-vector/minimal-geometric-stripe-shape-background_1409-1014.jpg',),
           fit:BoxFit.cover,
           colorFilter: ColorFilter.mode(
             Colors.black.withOpacity(0.4),
             BlendMode.darken,
           ),
         ),
       ),
       child: Center(
        child: SingleChildScrollView(
         child: Column(
           children: [
           Image(
          image: NetworkImage('https://cdn-icons-png.flaticon.com/512/6357/6357048.png',
           ),
             width:190,
           colorBlendMode: BlendMode.clear,
              ),
            SizedBox(
            height: 10,
    ),
           ListTile(
                title: Text(
                'Reset Password',
                style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 40.0,
                color: Colors.white,
                fontFamily: 'Courgette'
            ),
            textAlign: TextAlign.center,),
            subtitle: Text('   Please enter the code and enter the new password',
            style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 18.0,
            color: Colors.white,),
            textAlign: TextAlign.center,
            ),
            ),
            SizedBox(
            height: 10.0,
            ),
             Padding(
               padding: const EdgeInsets.only(
                   left: 60,
                   right: 60,
                   bottom: 8,
                   top:20
               ),
               child: TextFormField(
                 keyboardType:TextInputType.phone ,
                 style: TextStyle(
                   color: Colors.white,
                 ),
                 decoration: InputDecoration(
                   labelText: 'Enter the code',
                   labelStyle: TextStyle(
                     color: Colors.white70,
                   ),
                   prefixIcon: Icon(
                     Icons.quick_contacts_dialer,
                     color: Colors.white,),
                   border: OutlineInputBorder(),
                   enabledBorder: OutlineInputBorder(
                     borderSide: BorderSide(color: Colors.white,width:1.6),
                     borderRadius: BorderRadius.circular(20.0),
                   ),
                   focusedBorder:  OutlineInputBorder(
                     borderSide: BorderSide(color: Colors.white,width:1.6),
                     borderRadius: BorderRadius.circular(20.0),
                   ),
                 ),
               ),
             ),
             Padding(
               padding: const EdgeInsets.only(
                   left: 60,
                   right: 60,
                   top: 10,
                   bottom: 0
               ),
               child: TextFormField(
                 keyboardType: TextInputType.visiblePassword,
                 style: TextStyle(
                   color: Colors.white,
                 ),
                 decoration: InputDecoration(
                   labelText:'New Password',
                   labelStyle: TextStyle(
                     color: Colors.white70,
                   ),
                   border: OutlineInputBorder(),
                   enabledBorder: OutlineInputBorder(
                       borderSide: BorderSide(color: Colors.white,width:1.6),
                       borderRadius: BorderRadius.circular(20.0)
                   ),
                   focusedBorder:  OutlineInputBorder(
                     borderSide: BorderSide(color: Colors.white,width:1.6),
                     borderRadius: BorderRadius.circular(20.0),
                   ),
                   prefixIcon: Icon(Icons.lock,
                     color: Colors.white,),
                   suffixIcon: IconButton(
                     icon: isPasswordVisible?
                     Icon(Icons.visibility_off,
                       color: Colors.white,)
                         : Icon(Icons.visibility,
                       color: Colors.white,),
                     onPressed: () => setState(() => isPasswordVisible = !isPasswordVisible),
                   ),
                 ),
                 obscureText: isPasswordVisible,
               ),
             ),
             Row(
               mainAxisAlignment: MainAxisAlignment.end,
               children: [
                 Padding(
                   padding: const EdgeInsets.only(top: 20,
                       right: 20,
                       bottom: 40),
                   child: tripBtn(
                       content: 'Done',
                       icon: Icons.check_circle,
                       color: Colors.blue.withOpacity(.1),
                       myFunc: (){
                         Navigator.of(context).
                         push(MaterialPageRoute(builder: (context)=>ResetPassword()));
                       }),
                 ),
               ],
             ),
       ]
         )
    )
    ),
     )
    );
  }
  RaisedButton tripBtn(
      {required String content,required IconData icon,required Color color,required myFunc})
  {
    return RaisedButton(color: color,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
      child: Container(
        width: 120,
        height: 40,
        padding: EdgeInsets.symmetric(horizontal: 10,vertical: 5),
        child: Row(
          children: [
            Icon(
              icon,
              size: 28,
              color: Colors.white,
            ),
            SizedBox(
              width: 10,
            ),
            Text(
              content,
              style: TextStyle(
                  color:Colors.white ,
                  fontSize: 26),
            ),
          ],
        ),
      ),
      onPressed: myFunc,
    );
  }
}
