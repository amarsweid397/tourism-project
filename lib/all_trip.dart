import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:udemy_flutter/home_bar.dart';
import 'package:udemy_flutter/trip.dart';
import 'package:udemy_flutter/trips_made.dart';
class AllTrip extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size _size =MediaQuery.of(context).size;
    return Scaffold(
      body: ListView(
        children:[ ClipPath(
          clipper: CustomClipPath(),
          child: Container(
            color: Colors.white70,
            height: 400,
            child: Container(
            decoration: BoxDecoration(
            image: DecorationImage(
            image: NetworkImage('https://i.pinimg.com/564x/96/28/fd/9628fdba9736f35a6abc0c424b934e07.jpg'),
            fit: BoxFit.cover,
            )
    ),
    )
          )
        ),
          Text('All Trips ',
            style: TextStyle(
              color: Colors.blue,
            fontWeight: FontWeight.bold,
             fontFamily: 'Courgette',
            fontSize: 50,
          ),
            textAlign: TextAlign.center,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 2, left: 10.0, right: 10.0),
            child: ListView.separated(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemBuilder: (context, index) =>Column(
                  children: [
                    buildR(
                        content: 'Trips made',
                        icon: Icons.arrow_forward_ios,
                        images: 'https://previews.123rf.com/images/blankstock/blankstock1805/blankstock180501455/101832229-palm-tree-sign-icon-travel-trip-symbol-blurred-gradient-design-element-vivid-graphic-flat-icon-vecto.jpg',
                        myFunc:(){ Navigator.of(context).push(
                            MaterialPageRoute(builder: (context) => TripsMade()));}),
                    buildR(
                        content: 'Licensed trips',
                        icon: Icons.arrow_forward_ios,
                        images: 'https://static.thenounproject.com/png/2548936-200.png',
                        myFunc:(){  Navigator.of(context).push(
                            MaterialPageRoute(builder: (context) =>Trips()));} ),
                  ],
                ),
                separatorBuilder: (context, index) =>
                    SizedBox(
                      height: 20.0,
                    ),
                itemCount: 1),
          ),

    ],
      ),
    );
    // return Scaffold(
    // body: Column(
    // children:
    // [
    // Container(
    // decoration: BoxDecoration(
    // borderRadius: BorderRadiusDirectional.only(
    // bottomEnd: Radius.circular(50),
    // bottomStart: Radius.circular(50)),
    // ),
    // clipBehavior: Clip.antiAliasWithSaveLayer,
    // child: Stack(
    // alignment: Alignment.bottomCenter,
    // children:
    // [
    // Image(
    // image: NetworkImage('http://picsum.photos/500/500?random=3'),
    // width: double.infinity,
    // height: 300,
    // fit: BoxFit.cover,
    //
    // ),
    // Container(
    // width: 269,
    // color: Colors.black.withOpacity(.6),
    // padding: EdgeInsets.symmetric(
    // vertical: 10.0,
    // //horizontal: 10.0,
    // ),
    // child: Text(
    // 'Flower',
    // textAlign: TextAlign.center,
    // style: TextStyle(
    // fontSize: 20,
    // color: Colors.white,
    // ),
    // ),
    // ),
    // ],
    // ),
    // ),
    //
    // ],
    // ),
    // );
  }
}

class  CustomClipPath extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    double w = size.width;
    double h = size.height;
    final path = Path();
    path.lineTo(0, h);
    path.quadraticBezierTo(
      w*0.5,
        h-100,
        w,h);
    path.lineTo(w, 0);
    path.close();
    return path;
  }
  @override
  bool shouldReclip(CustomClipper<Path> oldClipper){
    return true;
  }

}
Widget buildR ({required String content,required IconData icon,required String images,required myFunc})=>
    Container(
      height: 80,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Card(
          color: Colors.white.withOpacity(.3),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(34)
          ),
          elevation: 10,
          shadowColor: Colors.grey.withOpacity(.4),
          child: ListTile(
            leading: Padding(
              padding: const EdgeInsets.only(top: 8,left: 16),
              child: CircleAvatar(radius: 22,
                backgroundColor: Colors.grey.withOpacity(.2),
                backgroundImage: NetworkImage(images),),
            ),
            title: Text(content, style: TextStyle(
              color: Colors.blue,
              fontWeight: FontWeight.bold,
              fontSize: 25,
            ),),
            trailing: Icon(Icons.arrow_forward_ios),
            onTap: myFunc,
          ),
        ),
    ),
    );


