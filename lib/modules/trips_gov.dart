// To parse this JSON data, do
//
//     final tripsGov = tripsGovFromJson(jsonString);

import 'dart:convert';

List<List<TripsGov>> tripsGovFromJson(String str) => List<List<TripsGov>>.from(json.decode(str).map((x) => List<TripsGov>.from(x.map((x) => TripsGov.fromJson(x)))));

String tripsGovToJson(List<List<TripsGov>> data) => json.encode(List<dynamic>.from(data.map((x) => List<dynamic>.from(x.map((x) => x.toJson())))));

class TripsGov {
    TripsGov({
       required this.name,
    });

    String name;

    factory TripsGov.fromJson(Map<String, dynamic> json) => TripsGov(
        name: json["name"],
    );

    Map<String, dynamic> toJson() => {
        "name": name,
    };
}
