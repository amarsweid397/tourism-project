// To parse this JSON data, do
//
//     final trips = tripsFromJson(jsonString);

import 'dart:convert';

Trips tripsFromJson(String str) => Trips.fromJson(json.decode(str));

String tripsToJson(Trips data) => json.encode(data.toJson());

class Trips {
    Trips({
     required   this.data,
    });

    Data data;

    factory Trips.fromJson(Map<String, dynamic> json) => Trips(
        data: Data.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "data": data.toJson(),
    };
}

class Data {
    Data({
      required this.id,
  required     this.name,
  required      this.tripDuration,
  required      this.cost,
   required     this.startsAt,
    required    this.endsAt,
    required    this.tripTime,
    required    this.details,
     required   this.touristGuideName,
     required   this.phoneNumber,
      required  this.evaluation,
    });
    int id;
    String name;
    int tripDuration;
    int cost;
    DateTime startsAt;
    DateTime endsAt;
    DateTime tripTime;
    String details;
    String touristGuideName;
    String phoneNumber;
    int evaluation;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
       id: json["id"],
        name: json["name"],
        tripDuration: json["trip_duration"],
        cost: json["cost"],
        startsAt: DateTime.parse(json["starts_at"]),
        endsAt: DateTime.parse(json["ends_at"]),
        tripTime: DateTime.parse(json["trip_time"]),
        details: json["details"],
        touristGuideName: json["tourist guide name"],
        phoneNumber: json["phoneNumber"],
        evaluation: json["evaluation"],
    );

    Map<String, dynamic> toJson() => {
        "name": name,
        "trip_duration": tripDuration,
        "cost": cost,
        "starts_at": "${startsAt.year.toString().padLeft(4, '0')}-${startsAt.month.toString().padLeft(2, '0')}-${startsAt.day.toString().padLeft(2, '0')}",
        "ends_at": "${endsAt.year.toString().padLeft(4, '0')}-${endsAt.month.toString().padLeft(2, '0')}-${endsAt.day.toString().padLeft(2, '0')}",
        "trip_time": tripTime.toIso8601String(),
        "details": details,
        "tourist guide name": touristGuideName,
        "phoneNumber": phoneNumber,
        "evaluation": evaluation,
    };
}
