import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:udemy_flutter/uitils_pro/custom_clipper.dart';
import 'package:udemy_flutter/widget_pro/list_guide.dart';
import 'package:udemy_flutter/data_pro/list_guide.dart';
import 'package:udemy_flutter/model_pro/list_guide.dart';

class TouristGuide extends StatefulWidget {
  const TouristGuide({Key? key}) : super(key: key);

  @override
  State<TouristGuide> createState() => _TouristGuideState();
}

class _TouristGuideState extends State<TouristGuide> {
  final listKey = GlobalKey<AnimatedListState>();
  final List<ListItem> items = List.from(listItems);
  @override
  Widget build(BuildContext context) {
    String guidName = '';
    String guidNumber = '';
    String guidRate = '';

    return Container(
      child: Scaffold(
        backgroundColor: Colors.grey[300],
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(88),
          child: AppBar(
            centerTitle: true,
            title: Text(
              '..Tourist Guide..',
              style: TextStyle(
                color: Colors.blue,
                fontSize: 36,
                fontWeight: FontWeight.bold,
              ),
            ),
            backgroundColor: Colors.transparent,
            elevation: 0.0,
            flexibleSpace: ClipPath(
              clipper: MyCustomClipperForAppBar(),
              child: Container(
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                  colors: [Colors.black.withOpacity(0.8), Color(0xFF292C36)],
                  begin: Alignment.bottomRight,
                  end: Alignment.topLeft,
                  tileMode: TileMode.clamp,
                )),
              ),
            ),
          ),
        ),
        body: AnimatedList(
          key: listKey,
          initialItemCount: items.length,
          itemBuilder: (context, index, animation) => ListItemWidget(
            item: items[index],
            animation: animation,
            onClicked: () => removedItem(index),
          ),
        ),
        floatingActionButton: Padding(
          padding: const EdgeInsets.only(bottom: 80),
          child: FloatingActionButton(
            onPressed: () {
              showDialog(
                  barrierDismissible: false,
                  context: context,
                  builder: (context) {
                    return AlertDialog(
                      title: Text('add new guid'),
                      content: Container(
                        height: 180,
                        child: Column(children: [
                          TextField(
                            onChanged: (value) => setState(() {
                              guidName = value;
                            }),
                            decoration: InputDecoration(
                              hintText: 'Guid Name',
                              border: OutlineInputBorder(),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.white,width:1.6),
                                borderRadius: BorderRadius.circular(20.0),
                              ),
                              focusedBorder:  OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.white,width:1.6),
                                borderRadius: BorderRadius.circular(20.0),
                              ),
                            ),
                          ),
                          TextField(
                            keyboardType: TextInputType.number,
                            onChanged: (value) => setState(() {
                              guidNumber = value;
                            }),
                            decoration: InputDecoration(
                              hintText: 'Guid Number',
                              border: OutlineInputBorder(),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.white,width:1.6),
                                borderRadius: BorderRadius.circular(20.0),
                              ),
                              focusedBorder:  OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.white,width:1.6),
                                borderRadius: BorderRadius.circular(20.0),
                              ),
                            ),
                          ),
                          TextField(
                            onChanged: (value) => setState(() {
                              guidRate = value;
                            }),
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                              hintText: 'Guid Rate',
                            ),
                          )
                        ]),
                      ),
                      actions: [
                        TextButton(
                            onPressed: () {
                              if (guidName != '' &&
                                  guidNumber != '' &&
                                  guidRate != '') {
                                insertItem(guidName, guidNumber, guidRate);
                              }
                              Navigator.of(context).pop();
                            },
                            child: Text('Ok ')),
                        TextButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: Text('Cancel '))
                      ],
                    );
                  });
            },
            backgroundColor: Colors.black,
            child: Icon(Icons.add),
          ),
        ),
      ),
    );
  }

  void insertItem(name, numebr, rate) {
    //مشان اذا لاضيف عنصلر بس
    // final newItem=ListItem(title: 'Orange',
    //     urlImage: 'https://es.la-croix.com/images/2000/que-es-el-derecho-natural.jpg');
    final newIndex =
        1; //اذا صفر فرح يبلش اضافة من البداية اما 1 فرح يبلش اضافة من بعد اول عنصر موجود
    //هون عم بضيف مننفس القائمة عناصر عشوائية منا من خلال .first
    final newItem = new ListItem(
        title: name,
        urlImage:
            'https://static.vecteezy.com/system/resources/thumbnails/007/230/224/small/tour-guide-line-icon-vector.jpg',
        number: numebr,
        rate: rate);
    items.insert(0, newItem);
    listKey.currentState!.insertItem(
      newIndex,
      duration: Duration(milliseconds: 600),
    );
  }

  void removedItem(int index) {
    final removedItem = items[index];
    items.removeAt(index);
    listKey.currentState!.removeItem(
      index,
      (context, animation) => ListItemWidget(
          item: removedItem, animation: animation, onClicked: () {}),
      duration: Duration(milliseconds: 600),
    );
  }
}
