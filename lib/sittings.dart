import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class SettingPage extends StatefulWidget {
  const SettingPage({Key? key}) : super(key: key);

  @override
  State<SettingPage> createState() => _SettingPageState();
}

class _SettingPageState extends State<SettingPage> {
  String? gender;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
      title: Text('Settings',style :TextStyle(fontSize:25,fontWeight: FontWeight.bold),
      textAlign:TextAlign.center,),
      centerTitle: false,
      leading: IconButton(icon: Icon(CupertinoIcons.arrow_left),onPressed: (){
      Navigator.of(context).pop();
    },),
    ),
   body: Column(
  children: [
    ListTile(
        title: Text("Night Mood",style: TextStyle(fontWeight:FontWeight.bold,fontSize: 20 ),),
        leading: Radio(
          value: "male", 
          groupValue: gender, 
          onChanged: (value){
            setState(() {
                gender = value.toString();
            });
          }),
    ),
     ListTile(
        title: Text("Sign Out",style: TextStyle(fontWeight:FontWeight.bold,fontSize: 20 ),),
        leading: Radio(
          value: "female", 
          groupValue: gender, 
          onChanged: (value){
            setState(() {
                gender = value.toString();
            });
          }),
    )

    
])    );

    
  }
}