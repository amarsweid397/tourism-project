

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:udemy_flutter/objects/end_trips.dart';
import 'package:udemy_flutter/services/http.dart';


import '../sources/group_trip_data.dart';
import 'booking_page.dart';
import 'modules/Trips.dart';
import 'modules/trips_gov.dart';

class GroupTripPage extends StatefulWidget {
 
   GroupTripPage({ Key? key }) : super(key: key);

  @override
  State<GroupTripPage> createState() => _GroupTripPageState();
}

class _GroupTripPageState extends State<GroupTripPage> {
    String dropdownvalue = "Lattakia";   
   HttpService service = new HttpService();
 List<dynamic>? trips;
   var lenth = 0 ;
   bool isLoading  =true;
  @override
 initState() {

    super.initState();
  getTripsData();
}
getTripsData ()async {
  trips = await service.getGovTrips(curr);
  if(trips!=null){
  setState(() {
      isLoading=false;
    lenth = trips!.length;
  });
  }

  }
  int curr = 5;
  var myList = [];
  // List of items in our dropdown menu
  var items = [    
  
 "Lattakia",
 "Homs",
 
 "Tartus",
 
  ];
int getIndex (String x ){
  int y =0;
 if (x== 
 "Lattakia"){
  y=4;
 }
  else if(x=="Tartus")
  y=5;
  else y=6;
  return y; 
}
  @override

  Widget build(BuildContext context) {
  
    // الصورة الخلفية
    return Container(
    
      decoration: BoxDecoration(image: DecorationImage(image: NetworkImage('https://picsum.photos/500/500?random=4')
      ,fit: BoxFit.cover,
      colorFilter: ColorFilter.mode(Colors.black.withOpacity(0.5), BlendMode.darken))),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            leading: IconButton(icon:Icon(CupertinoIcons.back,size: 35,color: Colors.white,),onPressed: (){
              Navigator.of(context).pop();
            },),
            actions: [
              IconButton(
               onPressed: (){
             showDialog(context: context, builder: (BuildContext context){
                  return AlertDialog(
                    title: Text('This interface displays the names of the collective nomads in Syria by governorate, and the details of each trip appear by clicking on it.',
                    style: TextStyle(fontSize:25,fontWeight: FontWeight.bold,color:Colors.teal  ),),
                    
                    );

                });


          },
               icon: Icon(CupertinoIcons.question_circle,color: Colors.white,size: 30,))
            
          ],),

          //العنوان

       body: ListView(
        shrinkWrap: true,
         children: [
           Center(child: Text(".. Group Trips ..",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 40,color: Colors.white),),)
         ,

         // زر الاختيارات
             Padding(
               padding: const EdgeInsets.symmetric(horizontal: 20),
               child: DropdownButton(
                    style:TextStyle(color: Colors.black,fontSize:20,fontWeight:FontWeight.bold  ) ,
                  // Initial Value
                  value: dropdownvalue,
                    
                  // Down Arrow Icon
                  icon: const Icon(Icons.keyboard_arrow_down,color: Colors.white,size:25),    
                    
                  // Array list of items
                  items: items.map((String items) {
                    return DropdownMenuItem(
                      value: items,
                      child: Text(items),
                    );
                  }).toList(),
                  // After selecting the desired option,it will
                  // change button value to selected value
                  onChanged: (String? newValue) async{
                                        setState(() {
                      dropdownvalue = newValue!;
                      curr =getIndex(newValue);
                      isLoading=true;
                    });
                    await getTripsData();
                  },
                ),
             ),

              // الرحلات
          // for (var i =0 ; i<trips.length;i++)
          //       groupTripBtn(content: trips[i].name,myfc: (){
          //       Navigator.of(context).push(MaterialPageRoute(builder: (context)=>BookingPage(trip :trips[i])));
          //    }),
        Visibility(
          visible: !isLoading,
          child:ListView.builder(
            
            itemCount:lenth,
            shrinkWrap: true,
            itemBuilder: (context,index){
           return  groupTripBtn(content: trips![index][0]['name'], myfc: ()async{
Trips? trip = await service.getTripData(trips![index][0]['id']);
if(trip!=null){
  Navigator.of(context).push(MaterialPageRoute(builder: (_)=>BookingPage(trip:trip ,)));
}
            });
            
          }),
          replacement:Center(child: CircularProgressIndicator(),))
             ],
       ),
      ),
    );
  }

  

  Padding groupTripBtn({
    required String content,required myfc
  }) {
    return Padding(
           padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 5),
           
           child: RaisedButton(
             padding: EdgeInsets.symmetric(vertical: 5,horizontal:10),
             child: Row(
               children: [
                 Text(content,textAlign: TextAlign.left,style: TextStyle(fontSize: 17),),

               
               ],
             ),
             onPressed: myfc,
               
             
           ),
         );
  }
}