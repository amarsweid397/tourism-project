import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:udemy_flutter/home_bar.dart';
import 'package:udemy_flutter/offers_admin.dart';
import 'package:udemy_flutter/places_screen.dart';
import 'package:udemy_flutter/services/http.dart';
import 'package:udemy_flutter/all_trip.dart';
import 'package:udemy_flutter/tourist_guide.dart';

import 'objects/guide.dart';

class HomeAdmin extends StatefulWidget {
  const HomeAdmin({Key? key}) : super(key: key);

  @override
  State<HomeAdmin> createState() => _HomeAdminState();
}

class _HomeAdminState extends State<HomeAdmin> {
  final navigationkey =GlobalKey<CurvedNavigationBarState>();

  
  int index =1;//مشان يبلش من النص التحديد(اول ماتفتح الشاشة)

  final screens = [
    //كل شاشة مرتبةحسب ترتيب الايقونات يعني اول وحدة هي home  فبقابلها الشاشة الخاصة فيها كمتن ترتيبها 1
  
    NaturalPage(),
    HomeBar(),
    OfferAdmin(),
    TouristGuide( ),

  ];
  @override
  Widget build(BuildContext context) {
    final items =<Widget>[
     
      Icon(Icons.account_balance_rounded,size: 30,),
      Icon(Icons.home,size: 30,),
      Icon(Icons.local_offer_outlined,size: 30,),
      Icon(Icons.group,size: 30,),
    ];
    return Scaffold(
      extendBody: true,//مشان ياخد الشريط يلي تحت خلفيةالصورة
      body: screens[index],
      // body: Center(
      //   child: Column(
      //     mainAxisAlignment: MainAxisAlignment.center,
      //     children: [
      //       Text('$index'),
      //       SizedBox(height: 16,),
      //       ElevatedButton(
      //           style: ElevatedButton.styleFrom(
      //             onPrimary: Colors.green,
      //             primary: Colors.deepPurpleAccent,
      //             maximumSize: Size(250, 60)
      //           ),
      //           onPressed: (){
      //             final navigationState =navigationkey.currentState!;
      //             navigationState.setPage(0);
      //           },
      //           child: Text('Go TO 0')),
      //
      //     ],
      //   ),
      // ),
      //body: screen[index],
      // body:Image.network('http://picsum.photos/500/500?random=40',
      // height:double.infinity ,
      // width: double.infinity,
      //   fit: BoxFit.cover,),
      //backgroundColor: Colors.blue,
      bottomNavigationBar: Theme(
        data: Theme.of(context).copyWith(
            iconTheme: IconThemeData(color:Colors.white)),//لون الايقونة
        child: CurvedNavigationBar(
           key: navigationkey,
          color: Colors.black,//لون كل الشريط
          buttonBackgroundColor: Colors.black,//لون خلفية الايقونة وقت وقف عليها
          backgroundColor: Colors.transparent,//لانو اللون الافتراضي ازرق اما هيك بصير حسب الخلفية
          items: items,
          index: index,//مشان يبلش بالنص عرفنا المتغير فوق
          height: 60,
          //هدول مشان نسرع حركة الانتقال بين العناصر
          // animationCurve: Curves.easeInOut,
          // animationDuration: Duration(microseconds:300),
          onTap: (index)=>setState(() {
            this.index=index;
          }),
        ),
      ),

    );
  }
}
