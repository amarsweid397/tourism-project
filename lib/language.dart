import 'dart:io';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../bottom_bar.dart';

class languagePage extends StatefulWidget {
  const languagePage({Key? key}) : super(key: key);

  @override
  State<languagePage> createState() => _languagePageState();
}

class _languagePageState extends State<languagePage> {
  int lang  =0;
    static       Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
   Future <void> SetLang(val)async {
  
    final  SharedPreferences prefs = await _prefs;
    await prefs.setInt('Lang', val);
Navigator.of(context).push(MaterialPageRoute(builder: (context) => BottomBar(index:0)));
 }
 Future <void > getLang () async {
 
var x = await _prefs.then((SharedPreferences prefs) => prefs.getInt('Lang')??0 ) ;
print('lang is $lang');
setState(() {
  lang = x;
});

 }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
   appBar: AppBar(
      title: Text('Select Language',style :TextStyle(fontSize:25,fontWeight: FontWeight.bold),
      textAlign:TextAlign.center,),
      centerTitle: false,
      leading: IconButton(icon: Icon(CupertinoIcons.arrow_left),onPressed: (){
      Navigator.of(context).pop();
    },),
    
    ),
    body: Column(children: [
       languageselect(content: 'Arabic',myfc: ()async{
               SetLang(1);
          print ('lang updated');
          getLang();
              //  exit(1);
             }),
             
              languageselect(content: 'English ',myfc: ()async{
  SetLang(0);
   print ('lang updated');
   getLang();
  
              //  exit(1);
                
              }),
             
            
    ],),

    );
    
  }
   Padding languageselect({
    required String content,required myfc
  }) {
    return Padding(
           padding: const EdgeInsets.symmetric(horizontal:20,vertical:5),
           child: RaisedButton(
            color: Colors.white,
             padding: EdgeInsets.symmetric(horizontal:5,),
             child: Row(
               children: [
                 Text(content,
                 style: TextStyle(fontSize: 17,fontWeight: FontWeight.bold),),

               
               ],
             ),
             onPressed: myfc,
               
             
           ),
         );
  }
}