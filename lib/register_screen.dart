import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:udemy_flutter/login_project.dart';
import 'package:udemy_flutter/services/http.dart';
import 'home_admin.dart';
import 'objects/user.dart';
class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  String password ='';
  var _Selected=0;
   
  bool isErr = false ; 
  bool isPasswordVisible = false;
  var emailController = TextEditingController();
  var passwordController = TextEditingController();
  var c_passwordController = TextEditingController();
  var nameController = TextEditingController();
  var numberController = TextEditingController();
  String nameErr = '';
  String numberErr = '';
  String emailErr = '';
  String passwordErr = '';
  String c_passwordErr = '';

  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/images/2.jpg'),
          fit:BoxFit.cover,
          colorFilter: ColorFilter.mode(
            Colors.black.withOpacity(0.6),
            BlendMode.darken,
          ),
        ),
      ),
        child:Scaffold( backgroundColor: Colors.transparent,
          body:Center(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 14),
                    child: Text(
                      'Create an account',textAlign: TextAlign.center,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 45.0,
                          color: Color(0xFF42aedc),
                          fontFamily: 'Courgette',
                      ),),
                  ),
                  Text('welcome with us',
                    style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 40.0,
                    color: Color(0xFF2AA8C0),
                    fontFamily: 'Courgette',
                  ),),
                  SizedBox(
                    height: 36.0,
                  ),
               errText(nameErr),
 Padding(
                    padding: const EdgeInsets.only(
                        left: 60,
                        right: 60,          
                    ),
                    child: TextFormField(
                      controller: nameController,
                      keyboardType:TextInputType.text,
                      style: TextStyle(
                        color: Colors.white,
                      ),
                      decoration: InputDecoration(
                        labelText: 'Name',
                        labelStyle: TextStyle(
                          color: Colors.white70,
                        ),
                        prefixIcon: Icon(
                          Icons.person,
                          color: Colors.white,),
                        border: OutlineInputBorder(),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white,width:1.6),
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                        focusedBorder:  OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white,width:1.6),
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                      ),
                    ),
                  ),
                  errText(numberErr),
                   Padding(
                    padding: const EdgeInsets.only(
                        left: 60,
                        right: 60,   
                      
                    ),
                    child: TextFormField(
                      controller: numberController,
                      keyboardType:TextInputType.number ,
                      style: TextStyle(
                        color: Colors.white,
                      ),
                      decoration: InputDecoration(
                        labelText: 'Phone Number',
                        labelStyle: TextStyle(
                          color: Colors.white70,
                        ),
                        prefixIcon: Icon(
                          Icons.phone,
                          color: Colors.white,),
                        border: OutlineInputBorder(),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white,width:1.6),
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                        focusedBorder:  OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white,width:1.6),
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                      ),
                    ),
                  ),
                               errText(emailErr),
                  Padding(
                    padding: const EdgeInsets.only(
                        left: 60,
                        right: 60, 
                    ),
                    child: TextFormField(
                      controller: emailController,
                      keyboardType:TextInputType.emailAddress ,
                      style: TextStyle(
                        color: Colors.white,
                      ),
                      decoration: InputDecoration(
                        labelText: 'Email Address',
                        labelStyle: TextStyle(
                          color: Colors.white70,
                        ),
                        prefixIcon: Icon(
                          Icons.email,
                          color: Colors.white,),
                        border: OutlineInputBorder(),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white,width:1.6),
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                        focusedBorder:  OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white,width:1.6),
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                      ),
                    ),
                  ),
                               errText(passwordErr),
                  Padding(
                    padding: const EdgeInsets.only(
                        left: 60,
                        right: 60,
                    ),
                    child: TextFormField(
                      controller: passwordController,
                      keyboardType: TextInputType.visiblePassword,
                      style: TextStyle(
                        color: Colors.white,
                      ),
                      decoration: InputDecoration(
                        labelText:'Password',
                        labelStyle: TextStyle(
                          color: Colors.white70,
                        ),
                        border: OutlineInputBorder(),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.white,width:1.6),
                            borderRadius: BorderRadius.circular(20.0)
                        ),
                        focusedBorder:  OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white,width:1.6),
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                        prefixIcon: Icon(Icons.lock,
                          color: Colors.white,),
                        suffixIcon: IconButton(
                          icon: isPasswordVisible?
                          Icon(Icons.visibility_off,
                            color: Colors.white,)
                              : Icon(Icons.visibility,
                            color: Colors.white,),
                          onPressed: () => setState(() => isPasswordVisible = !isPasswordVisible),
                        ),
                      ),
                      obscureText: isPasswordVisible,
                    ),
                  ),
                               errText(c_passwordErr),
                    Padding(
                    padding: const EdgeInsets.only(
                        left: 60,
                        right: 60,
                      
                        bottom: 0
                    ),
                    child: TextFormField(
                      
                      controller: c_passwordController,
                      keyboardType: TextInputType.visiblePassword,
                      style: TextStyle(
                        color: Colors.white,
                      ),
                      decoration: InputDecoration(
                        labelText:'confirm Password',
                        labelStyle: TextStyle(
                          color: Colors.white70,
                        ),
                        border: OutlineInputBorder(),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.white,width:1.6),
                            borderRadius: BorderRadius.circular(20.0)
                        ),
                        focusedBorder:  OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white,width:1.6),
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                        prefixIcon: Icon(Icons.lock,
                          color: Colors.white,),
                        suffixIcon: IconButton(
                          icon: isPasswordVisible?
                          Icon(Icons.visibility_off,
                            color: Colors.white,)
                              : Icon(Icons.visibility,
                            color: Colors.white,),
                          onPressed: () => setState(() => isPasswordVisible = !isPasswordVisible),
                        ),
                      ),
                      obscureText: isPasswordVisible,
                    ),
                  ),
                  SizedBox(
                    height:12.0,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 20,
                        
                            bottom: 40),
                        child: tripBtn(
                            content: 'Done',
                            icon: Icons.check_circle,
                            color: Color(0xFF0C4E57),
                            myFunc: ()async{
                              // Navigator.of(context).
                              // push(MaterialPageRoute(builder: (context)=>ResetPassword()));
                     if(nameController.text==''){
setState(() {
  nameErr="Name Is required !!";
  isErr=true;
});
                     }
                     else {
                      setState(() {
  nameErr="";
  isErr=false;
});
                     }
                         if(numberController.text==''){
setState(() {
  
    numberErr="Number  Is required !!";
    isErr=true;
});
                     }
                        else {
                      setState(() {
  numberErr="";
  isErr=false;
});
                     }
                           if(emailController.text==''){
setState(() {
  emailErr = 'Email Is required !!';
  isErr=true;
});
                     }
                        else {
                      setState(() {
  emailErr="";
  isErr=false;
});
                     }
                            if(passwordController.text==''){
setState(() {
  passwordErr='Password IS Required !!';
  isErr=true;
});
                     }
                        else {
                      setState(() {
  passwordErr="";
  isErr=false;
});
                     }
                            if(c_passwordController.text==''){
setState(() {
  
  c_passwordErr='Confirm your Password  !!';
  isErr=true;
});
                     }
                     else if(passwordController.text !=c_passwordController.text){
                      setState(() {
  
  c_passwordErr='Password dose not match   !!';
  isErr=true;
});
                     }
                        else {
                      setState(() {
  c_passwordErr="";
  isErr=false;
});
                     }
                     if (!isErr){
                      var user = new User(name: nameController.text,email: emailController.text 
                      
                      ,password: passwordController.text ,c_password: c_passwordController.text , 
                      number: numberController.text);
                      HttpService service = new HttpService();
                      await service.register(user);
            
                  Navigator.of(context).push(MaterialPageRoute(builder: (context)=>Test_Login()));
                  
              
            
                
                     }
                            }),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),

    );
  }

  RaisedButton tripBtn(
      {required String content,required IconData icon,required Color color,required myFunc})
  {
    return RaisedButton(color: color,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
      child: Container(
        width: 110,
        height: 50,
        padding: EdgeInsets.symmetric(horizontal: 10,vertical: 5),
        child: Row(
          children: [
            Icon(
              icon,
              size: 25,
              color: Colors.green,
            ),
            SizedBox(
              width: 6,
            ),
            Text(
              content,
              style: TextStyle(
                  color:Colors.white ,
                  fontSize: 24),
            ),
          ],
        ),
      ),
      onPressed: myFunc,
    );
  }
Text errText(String msg){
    return Text(msg , style: TextStyle(color: Colors.red,fontWeight: FontWeight.w700, fontSize: 20)
    );
  }
}
