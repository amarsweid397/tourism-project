
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:udemy_flutter/activity_user.dart';
import 'package:udemy_flutter/places.dart';
import 'package:udemy_flutter/resturant.dart';

import 'carbooking.dart';
import 'hotel.dart';
import 'location.dart';

class SingleTripPage extends StatefulWidget {
  const SingleTripPage({ Key? key }) : super(key: key);

  @override
  State<SingleTripPage> createState() => _SingleTripPageState();
}

class _SingleTripPageState extends State<SingleTripPage> {
  @override
  Widget build(BuildContext context) {
    Size _size = MediaQuery.of(context).size;
    return Container(
      
      decoration: BoxDecoration(image: DecorationImage(image: NetworkImage('https://picsum.photos/500/500?random=2')
      ,fit: BoxFit.cover,
      colorFilter: ColorFilter.mode(Colors.black.withOpacity(0.5), BlendMode.darken))),
      child: Scaffold(
        
        backgroundColor: Colors.transparent,
        appBar: AppBar(
            title: Text('Single Trips ',style: TextStyle(fontSize: 30,fontWeight: FontWeight.bold,color: Colors.white),),
          backgroundColor: Colors.transparent,
          elevation: 0,
          leading: IconButton(icon:Icon(CupertinoIcons.back,size: 35,color: Colors.white,),onPressed: (){
            Navigator.of(context).pop();
          },),
          actions: [
            IconButton(onPressed: (){

              Navigator.of(context).push(MaterialPageRoute(builder:(context)=>LocationPage()));
            }, icon: Icon(CupertinoIcons.location_north_fill,color: Colors.white,size: 30,))
          
        ],),
        body: 
            
            Container(
      padding:EdgeInsets.only(top: _size.height*0.05),
              child: GridView.count(crossAxisCount: 2,
              mainAxisSpacing: 10,
              children: [
              PopularWidget(size: _size, imageSrc: 'https://picsum.photos/500/500?random=2', name: 'Hotels',tapin:(){  Navigator.of(context).push(MaterialPageRoute(builder: (context)=>hotelPage()));} )
      ,    PopularWidget(size: _size, imageSrc: 'https://picsum.photos/500/500?random=7', name: 'Tourism Places',tapin:(){Navigator.of(context).push(MaterialPageRoute(builder: (context)=>TourismPlaces()));} )
             ,   PopularWidget(size: _size, imageSrc: 'https://picsum.photos/500/500?random=3', name: 'Activites',tapin:(){
              Navigator.of(context).push(MaterialPageRoute(builder: (context)=>ActivityUser()));
             })
      ,    PopularWidget(size: _size, imageSrc: 'https://picsum.photos/500/500?random=4', name: 'Cars Booking',tapin:(){  Navigator.of(context).push(MaterialPageRoute(builder: (context)=>CarBooking()));}),
        PopularWidget(size: _size, imageSrc: 'https://picsum.photos/500/500?random=5', name: 'Resturants',tapin:(){ Navigator.of(context).push(MaterialPageRoute(builder: (context)=>RestuPage()));})
       
              ],),),
          
        ),
        
      
    );
  }
  
   FlatButton  PopularWidget({
    
    required Size size,
    required String imageSrc,
 
    required String name,required tapin
    
  })
{
  return FlatButton(onPressed:tapin,
   child:Container(
     
     margin: EdgeInsets.symmetric(horizontal: 5,vertical: 5),
     decoration: BoxDecoration( borderRadius: BorderRadius.circular(30),image:DecorationImage(image:NetworkImage(imageSrc) ,fit: BoxFit.cover) ),
    width: size.width*0.54,height: 200,
  
       child:Padding(
         padding: const EdgeInsets.only(left:5,top: 60 ),
         child: Row(
           children: [
             Text(name,style: TextStyle(fontSize: 25,fontWeight:FontWeight.bold,color:Colors.white  ),),
           ],
         ),
       ) ,)
     

  

   );
    // return Stack(
    //   children: [
    //     Container(
          
    //       margin: EdgeInsets.symmetric(horizontal: 15),
    //       width:size.width*0.60,height: 200,
    //       child: Card(
    //         shape:  RoundedRectangleBorder(borderRadius: BorderRadius.circular(40)),
  
    //   child: ListTile(
      
    //   trailing: InkWell(
    //      onTap: tapin,
    //   child: Container( 
    //     child: Image(image: NetworkImage(imageSrc),fit: BoxFit.cover,),),

    //   ),
    //   title: Text(name,style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),

    //   ),
      
    //   ), 

    //       ),
    //   //     Positioned(
    //   //       top: 20,left: 30,
    //   //       child:Column(children: [
    //   //       Text(name,style:TextStyle(fontSize: 22,fontWeight: FontWeight.bold,color: Colors.white))
        
    //   //     ],))
      
    //    ],
    // );
  
}

}